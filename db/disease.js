'use strict';
const platformPlugin = require(__basedir + '/plugins/platform');
const stringUtils = require(__basedir + '/utils/string-utils');
const asset = require(__basedir + '/asset');

module.exports = (mongoose) => {

	let mySchema = mongoose.Schema({
		name: { type: String, required: true, maxlength: stringUtils.MAX_STRING_LENGTH, trim: true },
	})
		.plugin(platformPlugin);

	return mongoose.model('Disease', mySchema);
};
