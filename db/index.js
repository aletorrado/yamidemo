'use strict';
const mongoose = require('mongoose');
const config = require('config');

const asset = require(__basedir + '/asset');

mongoose.connect(config.get('mongodb'), {
	useNewUrlParser: true,
	useCreateIndex: true,
	bufferMaxEntries: 0,
	useUnifiedTopology: true,
});

module.exports = {
	Asset: asset.mongoose.model(mongoose),
	Disease: require('./disease')(mongoose),
	User: require('./user')(mongoose),
	mongoose: mongoose,
};
