'use strict';
const bcrypt = require('bcrypt'),
	lodash = require('lodash'),
	crypto = require('crypto'),
	SALT_WORK_FACTOR = 10,
	MIN_PASSWORD_LENGTH = 6,
	RESET_PASSWORD_TIME = 72 * 60 * 60 * 1000; //72 hours

const platformPlugin = require(__basedir + '/plugins/platform');
const foreignKey = require(__basedir + '/plugins/foreign_key');
const stringUtils = require(__basedir + '/utils/string-utils');
const asset = require(__basedir + '/asset');

module.exports = (mongoose) => {

	let mySchema = mongoose.Schema({
		email: { type: String, trim: true, match: [stringUtils.EMAIL_REGEX, 'Invalid email address.'], unique: true, required: true },
		password: { type: String },
		resetPassword: {
			token: { type: String, unique: true, sparse: true },
			expiresAt: { type: Date },
			_id: false
		},
		firstName: { type: String, required: true, maxlength: stringUtils.MAX_STRING_LENGTH, trim: true },
		lastName: { type: String, required: true, maxlength: stringUtils.MAX_STRING_LENGTH, trim: true },
		auth: [{
			accessToken: {
				type: String,
				unique: true,
				sparse: true
			},
			lastAccessAt: Date,
			_id: false
		}],
		photo: { type: mongoose.Schema.Types.ObjectId, ref: 'Asset' },
		systemAdmin: { type: Boolean, required: true, default: false },
		disabled: { type: Boolean, required: true, default: false },
	})
		.plugin(platformPlugin, { createdBy: false })
		.plugin(asset.mongoose.plugin)
		.plugin(foreignKey, {
			field: 'createdBy',
			models: ['Disease']
		});

	mySchema.virtual('displayName')
		.get(function () {
			return `${this.firstName} ${this.lastName[0]}.`;
		});

	mySchema.methods.comparePassword = function (candidatePassword, cb) {
		bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
			if (err) {
				return cb(err);
			}
			cb(null, isMatch);
		});
	};

	mySchema.methods.addAccessToken = function (callback) {
		crypto.randomBytes(16, (err, buf) => {
			if (err) {
				return callback(err);
			}
			let token = buf.toString('hex');
			this.auth.push({ accessToken: token });
			callback(null, { accessToken: token });
		});
	};

	mySchema.methods.removeAccessToken = function (accessToken, callback) {
		this.auth = lodash.filter(this.auth, (auth) => {
			return auth.accessToken !== accessToken;
		});
		callback();
	};

	mySchema.methods.resetPasswordToken = function (callback) {
		crypto.randomBytes(16, (err, buffer) => {
			if (err) {
				return callback(err);
			}

			let token = buffer.toString('hex');
			this.resetPassword = {
				token: token,
				expiresAt: Date.now() + RESET_PASSWORD_TIME
			};

			this.save((err) => {
				if (err) {
					return callback(err);
				}
				callback(null, token);
			});
		});
	};

	mySchema.pre('save', function (next) {
		if (!this.isModified('password')) {
			return next();
		}
		if (this.password.length < MIN_PASSWORD_LENGTH) {
			return next(new Error('Min password length error.'));
		}
		bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
			if (err) {
				return next(err);
			}
			bcrypt.hash(this.password, salt, (err, hash) => {
				if (err) {
					return next(err);
				}
				this.password = hash;
				next();
			});
		});
	});

	return mongoose.model('User', mySchema);
};
