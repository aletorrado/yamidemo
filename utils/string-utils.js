'use strict';
const unorm = require('unorm');

module.exports = {
	DATE_FORMAT: 'DD/MM/YYYY',
	DATE_TIME_FORMAT: 'DD/MM/YYYY HH:mm',
	MAX_STRING_LENGTH: 256,
	MAX_DESCRIPTION_LENGTH: 5000,
	MIN_PASSWORD_LENGTH: 6,
	MAX_URL_LENGTH: 2083,
	URL_REGEX: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/i,
	EMAIL_REGEX: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	NUMBER_REGEX: /^[0-9]*$/,
	normalize: (str) => {
		const combining = /[\u0300-\u036F]/g;
		return unorm.nfkd(str).replace(combining, '');
	},
	normalizeLowerCase: function (str) {
		return this.normalize(str).toLowerCase();
	},
	capitalizeFirstLetter: function (string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	rand: (length) => {
		const chars = '0123456789abcdefghijklmnopqrstuvwxyz';
		let string = '';
		for (let i = 0; i < length; i++) {
			let randomNumber = Math.floor(Math.random() * chars.length);
			string += chars.substring(randomNumber, randomNumber + 1);
		}
		return string;
	},
};
