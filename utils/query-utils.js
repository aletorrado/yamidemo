'use strict';
const lodash = require('lodash');
const mongoose = require('mongoose');
const moment = require('moment');

const stringUtils = require(__basedir + '/utils/string-utils');

module.exports.setDate = (date) => {
	if (!date) {
		return date;
	}
	return moment(date, stringUtils.DATE_FORMAT);
};

module.exports.ERRORS = {
	DUPLICATED_EMAIL: 'DUPLICATED_EMAIL',
	DUPLICATED: 'DUPLICATED',
	FOREIGN_KEY: 'FOREIGN_KEY',
	MAX_ATTEMPTS: 'MAX_ATTEMPTS',
	DISABLED: 'DISABLED',
	PENDING_RECOVERY: 'PENDING_RECOVERY',
};

module.exports.DUPLICATED_CODE = 11000;
module.exports.PAGE_SIZE = 20;

const _escapeRegExp = (str) => {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
};

module.exports.genSearchCondition = (search, fields) => {
	if (!search) {
		return {};
	}
	let terms = search.split(' ');
	let ands = [];
	terms.forEach((term) => {
		let regex = new RegExp(_escapeRegExp(term), 'i');
		let ors = [];
		fields.forEach((field) => {
			let condition = {};
			condition[field] = {$regex: regex};
			ors.push(condition);
		});
		ands.push({$or: ors});
	});
	return {
		$and: ands
	};
};

const isObject = (x) => {
	return lodash.isObject(x) && !lodash.isDate(x);
};

module.exports.serialize = (doc, db, model, fields) => {
	if (!doc) {
		return doc;
	}
	// var data = doc.toJSON({virtuals: true, getters: true});
	let data = doc;
	if (fields) {
		data = lodash.pick(data, fields);
	}
	data.id = doc.id; // this forces to output id ?
	const fix = (data, path, embedded) => {
		// FIXME: better check for asset
		if (data && isObject(data) && data.files && data.refs) {
			return {
				id: data._id,
				error: data.error,
				files: lodash.map(data.files, (x) => {
					return {
						url: x.url,
						size: x.size,
						metadata: x.metadata,
						tag: x.tag,
					}
				})
			}
		}
		if (!isObject(data) || data instanceof mongoose.Types.ObjectId) {
			return data;
		}

		if (Array.isArray(data)) {
			let embedded = db[model].schema.path(path) instanceof mongoose.Schema.Types.DocumentArray;
			return lodash.map(data, (value, key) => {
				return fix(value, lodash.chain([path, key]).compact().join('.').value(), embedded)
			});
		}
		if (data === null || data === undefined) {
			return data;
		}
		if (data instanceof mongoose.Document) {
			data = data.toJSON({virtuals: true, getters: true});
		}
		data = lodash.mapValues(data, (value, key) => {
			return fix(value, lodash.chain([path, key]).compact().join('.').value())
		});
		if (embedded) {
			delete data.id;
		} else {
			data.id = data.id || data._id;
			delete data._id;
			delete data.__v;
		}
		return data;
	};

	data = fix(data);
	return data;
};

module.exports.fixData = (data) => {
	const fix = (data) => {
		let out = lodash.mapValues(data, (x) => {
			if (Array.isArray(x)) {
				return lodash.compact(lodash.map(x, (item) => {
					return isObject(item) ? fix(item) : item;
				}));
			}
			if (x && isObject(x)) {
				return fix(x);
			}
			if (!lodash.isNull(x) && x !== '') {
				return x;
			}
		});
		if (data.id) {
			out._id = data.id;
		}
		return out;
	};

	const removeUndefinedObject = (obj) => {
		let isAllUndefined = true;

		for (let key in obj) {
			if (isObject(obj[key])) {
				obj[key] = removeUndefinedObject(obj[key]);
			}
			if (!lodash.isUndefined(obj[key])) {
				isAllUndefined = false;
			}
		}

		if (isAllUndefined) {
			obj = lodash.isArray(obj) ? [] : undefined;
		}
		return obj;
	};

	data = fix(data);
	removeUndefinedObject(data);
	return data;
};
