'use strict';
const express = require('express');
const mongoose = require('mongoose');
const lodash = require('lodash');

const queryUtils = require('./query-utils');

const ensureFunction = (obj) => {
	if (typeof obj === 'function') {
		return obj;
	}
	return () => {
		return obj;
	};
};

const cursorFromDoc = (doc, sort) => {
	if (!doc) {
		return;
	}
	return lodash.chain(sort)
		.split(' ')
		.map(x => x.replace('-', ''))
		.map(field => doc.get(field))
		.map(value => {
			return value instanceof Date ? value.getTime() : value;
		})
		.map(encodeURIComponent)
		.join('|')
		.thru(str => Buffer.from(str).toString('base64').replace(/=/g, ''))
		.value();
};

const conditionsFromCursor = (cursor, sort, isNextCursor) => {
	isNextCursor = !!isNextCursor;
	const sortsData = lodash.chain(sort)
		.split(' ')
		.map(item => ({
			desc: item[0] === '-',
			field: item.replace(/^-/, ''),
		}))
		.value();
	return lodash.chain(cursor)
		.thru(str => Buffer.from(str, 'base64').toString())
		.split('|')
		.map(decodeURIComponent)
		.map((value, idx) => {
			const currentSort = sortsData[idx];
			let modifier = currentSort.desc !== isNextCursor ? '$gt' : '$lt';
			if (idx < sortsData.length-1) {
				modifier += 'e';
			}
			let condition = {};
			condition[modifier] = value;
			return [
				currentSort.field,
				condition,
			];
		})
		.fromPairs()
		.value()
};

const getPopulables = (schema) => {
	let accumulator = [];
	lodash.chain(schema.paths)
		.mapValues((value, key) => {
			if (value.options.ref) {
				accumulator.push({path: key, ref: value.options.ref});
			} else if (value.caster && value.caster.options.ref) {
				accumulator.push({path: key, ref: value.caster.options.ref});
			} else if (value.caster && value.caster.schema) {
				accumulator.push(...getPopulables(value.caster.schema).map(item=>({path: `${key}.${item.path}`, ref: item.ref})));
			}
		})
		.pickBy()
		.value();
	return accumulator;
};

const getPopulablesOptions = (req, config, modelDef) => {
	return lodash.chain(modelDef.populableFields)
		.map((populable) => {
			let refModelDef = lodash.find(config, {id: populable.ref});
			let match = {};
			if (refModelDef) {
				lodash.assign(match, refModelDef.filterQuery(req));
			}
			return [
				populable.path,
				{match: match,},
			];
		})
		.fromPairs()
		.value()
};

const deconstructPath = (pathname) => {
	let parts = pathname.split('.');
	if (parts.length === 1) {
		return [pathname];
	}
	return [parts.join('.'), ...deconstructPath(parts.slice(0, -1).join('.'))];
};

const findPath = (schema, pathname) => {
	for (let pathnamepart of deconstructPath(pathname)) {
		if (pathnamepart in schema.paths) {
			let pathpart = schema.paths[pathnamepart];
			if (pathpart.schema) {
				return findPath(pathpart.schema, pathname.slice(pathnamepart.length + 1));
			}
			return pathpart;
		}
	}
};

module.exports = (db, systemAdmin, config, defaults) => {

	config = config.map(modelDef => {
		let model = db[modelDef.modelName || modelDef.id];
		let allFields = lodash
			.chain(model.schema.paths)
			.keys()
			.filter(x => !lodash.startsWith(x, '_'))
			.difference(modelDef.excludeFields || [])
			.value();
		modelDef = lodash.assign({
			sorts: [],
			searchableFields: [],
			textSearch: false,
			single: false,
			model: model,
			endpoint: '/' + lodash.kebabCase(modelDef.id),
			populableFields: getPopulables(model.schema),
			filterableFields: [],
			canView: true,
			canEdit: true,
			canDelete: true,
			canInsert: true,
			readOnly: false,
			unlimited: false,
			modelHandlers: {
				get: true,
				post: true
			},
			documentHandlers: {
				get: true,
				post: true,
				delete: true
			},
			owner: true
		}, defaults, modelDef);
		modelDef.filterQuery = ensureFunction(modelDef.filterQuery);
		modelDef.canView = ensureFunction(modelDef.canView);
		modelDef.canEdit = ensureFunction(modelDef.canEdit);
		modelDef.readOnly = ensureFunction(modelDef.readOnly);
		modelDef.canDelete = ensureFunction(modelDef.canDelete);
		modelDef.canInsert = ensureFunction(modelDef.canInsert);
		modelDef.defaults = lodash.isFunction(modelDef.defaults) ? modelDef.defaults  : () => lodash.cloneDeep(modelDef.defaults || {});
		modelDef.unlimited = ensureFunction(modelDef.unlimited);
		modelDef.modelHandlers = lodash.mapKeys(modelDef.modelHandlers, (value, key) => key.toUpperCase());
		modelDef.documentHandlers = lodash.mapKeys(modelDef.documentHandlers, (value, key) => key.toUpperCase());
		modelDef.viewableFields = modelDef.viewableFields
			|| modelDef.editableFields && lodash.difference(modelDef.editableFields, modelDef.excludeFields)
			|| allFields;
		modelDef.viewableFields = lodash.union(['id'], modelDef.viewableFields, modelDef.editableFields);
		modelDef.editableFields = modelDef.editableFields || modelDef.viewableFields;
		modelDef.editableFields = lodash.difference(modelDef.editableFields, ['id']);
		modelDef.picker = modelDef.picker || ((data) => {
			const newData = {};
			modelDef.viewableFields.forEach(field => {
				lodash.set(newData, field, lodash.get(data, field));
			});
			return newData;
		});
		modelDef.unpicker = modelDef.unpicker || ((doc, req) => {
			modelDef.editableFields.forEach(field => {
				const value = lodash.get(req.body, field);
				if (value !== undefined) {
					lodash.set(doc, field, value);
				}
			});
		});
		return modelDef;
	});

	const serialize = (data, req) => {
		if (!req) {
			throw new Error('Missing request context.');
		}
		if (typeof data !== 'object' || data instanceof Date || data instanceof mongoose.Types.ObjectId) {
			return data;
		}
		if (data && typeof data === 'object' && data._bsontype === 'Binary') {
			return data.buffer.toString()
		}
		if (Array.isArray(data)) {
			return lodash.map(data, (value, key) => {
				return serialize(value, req)
			});
		}
		if (data === null || data === undefined) {
			return data;
		}
		if (data instanceof mongoose.Document) {
			if (data.constructor.modelName) {
				let itemDef = lodash.find(config, {id: data.constructor.modelName});
				if (itemDef) {
					data = itemDef.picker(data, req, itemDef.viewableFields, (x)=>serialize(x, req));
				} else {
					// no model definition
					data = lodash.pick(data, ['id']);
				}
			} else {
				return lodash.mapValues(data.toJSON({getters: true, virtuals: true}), (value, key) => {
					return serialize(data[key], req);
				});
			}
		}
		data = lodash.mapValues(data, (value, key) => {
			return serialize(value, req)
		});
		return data;
	};

	let outerRouter = express.Router();

	for (const modelDef of config) {
		if (!modelDef.endpoint) {
			continue;
		}

		const modelRouter = express.Router();

		modelRouter.use((req, res, next) => {
			req.db = db;
			next();
		});

		if (modelDef.modelRouter) {
			modelRouter.use(modelDef.modelRouter);
		}

		modelRouter.use((req, res, next) => {
			if (systemAdmin && !req.user.systemAdmin) {
				return res.sendStatus(403);
			}
			next();
		});

		const model = modelDef.model;

		if (!modelDef.single && modelDef.modelHandlers.GET) {
			modelRouter.get('/', (req, res, next) => {
				let skip = parseInt(req.query.skip);
				if (skip < 0) {
					return next(new Error('Invalid skip.'));
				}

				let limit = parseInt(req.query.limit);
				if (limit) {
					if (limit < 0 || limit > 50) {
						return next(new Error(`Invalid limit: ${limit}`));
					}
				} else {
					let unlimited = modelDef.unlimited(req);
					if (!unlimited) {
						return next(new Error('Unlimited not allowed.'));
					}
				}

				let conditions = {$and: []};
				for (let [field, fieldValue] of Object.entries(req.query)) {
					let fieldName = field.split('_')[0];
					if (modelDef.filterableFields.indexOf(fieldName) === -1) {
						continue;
					}
					let fieldVariant = field.split('_')[1];
					if (fieldVariant) {
						if (typeof conditions[fieldName] !== 'object') {
							conditions[fieldName] = {};
						}
						conditions[fieldName]['$' + fieldVariant] = fieldValue;
					} else {
						conditions[fieldName] = fieldValue;
					}
				}
				if (modelDef.filterQuery) {
					let filterQuery = modelDef.filterQuery(req);
					if (filterQuery) {
						conditions.$and.push(filterQuery);
					}
				}
				if (!systemAdmin) {
					conditions.$and.push({createdBy: req.user.id});
				}
				if (modelDef.textSearch && (req.query.search || req.query.autocomplete && !modelDef.searchableFields.length)) {
					conditions.$text = { $search: req.query.search || req.query.autocomplete };
				} else if (modelDef.searchableFields.length && (req.query.autocomplete || req.query.search)) {
					conditions.$and.push(...queryUtils.genSearchCondition(req.query.autocomplete || req.query.search, modelDef.searchableFields).$and);
				}
				if (!conditions.$and.length) {
					delete conditions.$and;
				}
				let sort;
				if (req.query.sort && modelDef.sorts.indexOf(req.query.sort) >= 0) {
					sort = req.query.sort;
				} else if (modelDef.sorts.length) {
					sort = modelDef.sorts[0];
				}
				if (req.query.prev_cursor) {
					lodash.assign(conditions, conditionsFromCursor(req.query.prev_cursor, sort))
				}
				if (req.query.next_cursor) {
					lodash.assign(conditions, conditionsFromCursor(req.query.next_cursor, sort, true))
				}
				let populableOptions = getPopulablesOptions(req, config, modelDef);
				Promise.all([
					model.find(conditions)
						.skip(skip)
						.limit(limit)
						.sort(sort)
						.deepPopulate(lodash.keys(populableOptions), {
							populate: populableOptions,
						})
						.exec()
						.then(docs => {
							let firstDoc = docs[0];
							let lastDoc = docs.length && docs[docs.length-1];
							let prevCursor = cursorFromDoc(firstDoc, sort);
							let nextCursor = cursorFromDoc(lastDoc, sort);
							return {
								prevCursor: prevCursor,
								nextCursor: nextCursor,
								data: lodash.map(docs, doc => serialize(doc, req)),
							};
						}),
					model.countDocuments(conditions)
						.exec()
						.then(count => {
							return {count: count};
						})
				])
				.then(data => {
					res.send(lodash.assign.apply(null, data));
				})
				.catch(next);
			});

		}

		if (!modelDef.single && modelDef.modelHandlers.POST) {
			modelRouter.post('/', (req, res, next) => {
				if (!modelDef.canInsert(req) || modelDef.readOnly(req)) {
					return res.sendStatus(403);
				}
				let doc = new model(modelDef.defaults(req));
				modelDef.unpicker(doc, req);
				doc.createdBy = req.user;
				let populableOptions = getPopulablesOptions(req, config, modelDef);
				doc.save()
					.then(doc => doc.deepPopulate(lodash.keys(populableOptions), {populate: populableOptions}))
					.then(doc => res.send(serialize(doc, req)))
					.catch(next);
			});
		}

		const docRouter = express.Router();

		if (modelDef.docRouter) {
			docRouter.use(modelDef.docRouter);
		}

		modelDef.documentHandlers.GET && docRouter.get('/', (req, res, next) => {
			if (!modelDef.canView(req)) {
				return res.sendStatus(403);
			}

			res.send(serialize(req.document, req));
		});

		docRouter.use((req, res, next) => {
			if (modelDef.readOnly(req)) {
				return res.sendStatus(403);
			}

			next();
		});

		modelDef.documentHandlers.POST && docRouter.post('/', (req, res, next) => {
			if (!modelDef.canEdit(req)) {
				return res.sendStatus(403);
			}

			modelDef.unpicker(req.document, req);
			let populableOptions = getPopulablesOptions(req, config, modelDef);
			req.document.save()
				.then(doc => doc.deepPopulate(lodash.keys(populableOptions), {populate: populableOptions}))
				.then(doc => {
					res.send(serialize(req.document, req));
				})
				.catch(next);
		});

		modelDef.documentHandlers.DELETE && docRouter.delete('/', (req, res, next) => {
			if (!modelDef.canDelete(req)) {
				return res.sendStatus(403);
			}

			req.document.remove()
				.then(() => {
					res.send();
				})
				.catch(next);
		});

		modelRouter.use(modelDef.single ? '/' : '/:id', (req, res, next) => {
			let conditions = {};
			if (req.params.id) {
				conditions._id = req.params.id;
			}
			if (!systemAdmin) {
				conditions.createdBy = req.user.id;
			}
			let filterQuery = modelDef.filterQuery(req);
			if (filterQuery) {
				conditions.$and = [filterQuery];
			}
			let populableOptions = getPopulablesOptions(req, config, modelDef);
			model.findOne(conditions)
				.deepPopulate(lodash.keys(populableOptions), {
					populate: populableOptions,
				})
				.exec()
				.then(doc => {
					if (!doc && !modelDef.single) {
						res.sendStatus(404);
						return;
					}
					req.document = doc || new model({});
					next();
				})
				.catch(next)
		}, docRouter);

		outerRouter.use(modelDef.endpoint, modelRouter);

	}

	return outerRouter;

};
