
const escapeHtml = (unsafe) => {
	return unsafe
	 .replace(/&/g, "&amp;")
	 .replace(/</g, "&lt;")
	 .replace(/>/g, "&gt;")
	 .replace(/"/g, "&quot;")
	 .replace(/'/g, "&#039;");
 };

class SafeString {
	constructor(str) {
		this.str = str;
	}
	toString(){
		return this.str;
	}
	inspect(){
		return this.str;
	}
}

const html = (parts) => {
	let result = '';
	parts.forEach((part, idx)=>{
		if (idx > 0) {
			const mixin = arguments[idx];
			if (mixin instanceof SafeString) {
				result += mixin;
			} else {
				result += escapeHtml(new String(mixin));
			}
		}
		result += part;
	});
	return new SafeString(result);
};

const dom = () => {
	const code = html.apply(this, arguments);
	const parser = new DOMParser();
	const fragment = parser.parseFromString(code, 'text/html');
	return fragment.body.firstElementChild;
};

module.exports.escapeHtml = escapeHtml;
module.exports.SafeString = SafeString;
module.exports.html = html;
module.exports.dom = dom;
