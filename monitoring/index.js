'use strict';
const lodash = require('lodash');
const cheerio = require('cheerio');
const config = require('config');

const mailer = require(__basedir + '/mailer');

const EXPRESS_REPORT_TIMEOUT = 120 * 1000;
const MIN_DELAY = 60000;

module.exports = () => {
	let sendReport = lodash.throttle((report) => {
		mailer.send({
			to: config.get('mailer.testing'),
			subject: report.title,
			monitoring: true,
			html: lodash.chain(report.sections).map((section) => {
				let html = cheerio('<div style="margin-bottom: 10px;"><div id="name" style="font-weight: bold;"></div><div id="content" style="font-family: Courier New, Courier, monospace;"></div></div>');
				html.find('#name').text(section.name);
				let content = (typeof section.content == 'string' ? section.content : JSON.stringify(section.content, null, 4));
				if (content) {
					content = content.replace(/ /g, '&nbsp;')
						.replace(/\n/g, '<br />')
						.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
					html.find('#content').html(content);
				}
				return html;
			}).value().join('')
		}, (err) => {
			if (err) {
				console.error(err, report.title, report.sections);
			}
		});
	}, MIN_DELAY);

	return {
		express: (opts) => {
			opts = lodash.defaults(opts || {}, {
				timeout: EXPRESS_REPORT_TIMEOUT
			});
			return (req, res, next) => {
				let startDate = new Date();
				let onFinish = lodash.once(() => {
					let finishDate = new Date();
					let timeDiff = finishDate - startDate;
					let isTimeout = timeDiff >= opts.timeout && req.url.indexOf('/assets/') == -1;
					let isError = (res.statusCode + '')[0] == '5';
					if (!isError && !isTimeout) {
						return;
					}
					if (isError) {
						let report = {
							title: '[' + config.get('name') + '] [' + (isError ? 'ERROR' : 'TIMEOUT') + '] [' + new Date().toISOString() + ']',
							sections: lodash.compact([
								isTimeout ? {
									name: 'Time',
									content: timeDiff + ' ms'
								} : null,
								{
									name: 'Method',
									content: req.method
								},
								{
									name: 'Url',
									content: req.url
								},
								{
									name: 'Hostname',
									content: require('os').hostname()
								},
								{
									name: 'Client IP',
									content: req.ip
								},
								{
									name: 'Stack',
									content: res.error && res.error.stack || 'No data'
								},
								{
									name: 'Message',
									content: res.error && res.error.message || 'No data'
								},
								{
									name: 'Mongoose errors',
									content: res.error && res.error.errors || 'No data'
								},
								{
									name: 'Headers',
									content: lodash.chain(req.headers).toPairs().map((x) => {
										return x.join(': ')
									}).value().join('\n')
								},
								{
									name: 'Body',
									content: req.body
								}
							])
						};
						sendReport(report);
					} else {
						console.error('TIMEOUT', req.url);
					}
				});
				res.on('finish', onFinish);
				setTimeout(onFinish, opts.timeout);
				next();
			}
		},
		mongoose: (instance) => {
			instance.connection.on('error', (err) => {
				let report = {
					title: '[' + process.ENV.APP_NAME + '] [ERROR] [mongoose] [Connection Error] [' + new Date().toISOString() + ']',
					sections: [
						{
							name: 'Hostname',
							content: require('os').hostname()
						},
						{
							name: 'Stack',
							content: err.stack
						}
					]
				};
				sendReport(report);
				// Mongoose will reconnect automatically
				instance.disconnect();
			});
		}
	}
};
