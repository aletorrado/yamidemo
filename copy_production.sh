#!/bin/bash
gulp
rsync -avz \
--exclude 'dump.rdb' \
--exclude '/_*' \
--exclude /node_modules \
--exclude /public/assets \
--exclude .DS_Store \
--exclude src \
--exclude /.git \
--exclude '/*.sh' \
--exclude /.idea
. \
test@99uno.com:webserver/
ssh test@99uno.com ' pm2 restart all'
EOF
