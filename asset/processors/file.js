'use strict';

module.exports = (opts, callback) => {
	callback(null, [
		{
			stream: opts.file,
			extension: opts.filename.split('.').slice(-1)[0],
			// tag: '',
			tag: opts.filename.split('.').slice(0, -1).join('.'),
			mime: opts.mime,
		},
	]);
};
