'use strict';
const async = require('async');
const lodash = require('lodash');
const stream = require('stream');
const collect = require('stream-collect');
const pngquant = require('pngquant');
const sharp = require('sharp');
const config = require('config');

module.exports = (opts, callback) => {
	opts.file.setMaxListeners(64);
	let inlineOption = {
		format: 'png',
		size: [16, 16],
	};
	let options = [inlineOption].concat(opts.options);
	async.map(options, (opt, callback) => {
		let worker = sharp();
		if (opt.type === 'thumbnail') {
			worker
				.resize(opt.size[0], opt.size[1])
				.crop(sharp.gravity.center)
		} else if (opt.size) {
			worker
				.resize(opt.size[0], opt.size[1])
				.max()
				.withoutEnlargement()
		}
		if (opt.format === 'png') {
			worker.png({
				progressive: true,
			});
		} else if (opt.format === 'jpeg' || opt.format === 'jpg') {
			worker.jpeg({
				progressive: true,
				quality: 85,
			})
		} else if (opt.format === 'webp') {
			worker.webp({
				quality: 85,
				// alphaQuality?
			})
		}
		let pt1 = new stream.PassThrough();
		let pt2 = new stream.PassThrough();
		opts.file.pipe(worker)
			.on('error', err => {
				config.get('debug') && console.error(err);
				throw err;
			});
		worker.pipe(pt1);
		worker.pipe(pt2);
		pt1.pipe(sharp())
			.metadata()
			.then(data => {
				let metadata = {
					size: [data.width, data.height],
					format: data.format,
				};
				callback(null, {
					mime: ['image', data.format.toLowerCase()].join('/'),
					stream: pt2,
					tag: opt.tag,
					extension: opt.format,
					metadata: metadata,
				});
			})
			.catch(callback)
	}, (err, data) => {
		if (err) {
			return callback(err);
		}
		let inlineResult = data[0];
		let streams = data.slice(1);
		let inlineThrough = new stream.PassThrough();
		inlineResult.stream.pipe(new pngquant([16])).pipe(inlineThrough);
		collect(inlineThrough)
			.then(inlineData => {
				callback(null, {
					streams: streams,
					metadata: {
						inline: inlineData,
					},
				});
			})
			.catch(callback);
	});
};
