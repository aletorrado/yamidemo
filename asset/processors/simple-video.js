'use strict';
const stream = require('stream');
const mime = require('mime-types');
const path = require('path');
const lodash = require('lodash');
const tmp = require('tmp');
const fs = require('fs');
const childProcess = require('child_process');

module.exports = (opts, callback) => {
	let inStream = opts.file;
	let extension = opts.filename.split('.').slice(-1)[0];
	if (extension === opts.filename) {
		extension = '';
	}
	let videoFilename = tmp.tmpNameSync();
	inStream
		.pipe(fs.createWriteStream(videoFilename))
		.on('finish', () => {
			let tmpThumbFilename = tmp.tmpNameSync();
			let cp = childProcess.spawn(path.join(__dirname, '..', 'ffmpeg-linux-64bits', 'ffmpeg'), [
				'-i', videoFilename,
				'-vframes', '1',
				'-f', 'mjpeg',
				tmpThumbFilename,
			]);
			// cp.stdout.pipe(process.stdout)
			// cp.stderr.pipe(process.stderr)
			cp.on('exit', (code) => {
				if (code) {
					callback(new Error('FFmpeg error'));
					return;
				}
				childProcess.execFile(path.join(__dirname, '..', 'ffmpeg-linux-64bits', 'ffprobe'), [
					'-print_format',
					'json',
					'-v', 'quiet',
					'-show_streams',
					videoFilename
				], (err, stdout, stderr) => {
					let data = JSON.parse(stdout);
					let videoStream = lodash.find(data.streams, {codec_type: 'video'});
					// console.log(videoStream)
					let duration = parseFloat(data.streams[0].duration);
					let videoReadStream = fs.createReadStream(videoFilename);
					let thumbReadStream = fs.createReadStream(tmpThumbFilename);
					callback(null, {
						finish: () => {
							try {
								fs.unlinkSync(videoFilename);
							} catch (e) {}
							try {
								fs.unlinkSync(tmpThumbFilename);
							} catch (e) {}
						},
						// metadata: videoStream,
						streams: [
							{
								stream: videoReadStream,
								mime: extension && mime.lookup(extension) || 'application/octet-stream',
								tag: 'video',
								metadata: {
									duration: duration,
								},
								extension: extension,
							},
							{
								stream: thumbReadStream,
								mime: 'image/jpeg',
								tag: 'thumb',
								extension: 'jpg',
							},
						],
					});
				});
			});
		});
};
