'use strict';

module.exports = {
	image: require('./image'),
	// audio: require('./audio'),
	simpleVideo: require('./simple-video'),
	transcodeVideo: require('./transcode-video'),
	file: require('./file'),
};
