'use strict';
const mime = require('mime-types');
const path = require('path');
const lodash = require('lodash');
const tmp = require('tmp');
const fs = require('fs');
const childProcess = require('child_process');

module.exports = (opts, callback) => {
	let inStream = opts.file;
	let inputFilename = tmp.tmpNameSync();
	inStream
		.pipe(fs.createWriteStream(inputFilename))
		.on('error', err => {
			callback(err);
		})
		.on('finish', () => {
			let transcodeFilename = tmp.tmpNameSync();
			let videoCp = childProcess.spawn('nice', [
				'-n', '19',
				path.join(__dirname, '..', 'ffmpeg-linux-64bits', 'ffmpeg'),
				'-i', inputFilename,
				// '-c:v', 'copy', '-c:a', 'copy',
				'-c:v', 'libx264',
				'-b:v', '0.8M',
				'-c:a', 'aac',
				'-b:a', '64K',
				'-preset', 'veryfast',
				'-f', 'mp4',
				transcodeFilename,
			], {
				stdio: 'ignore',
			});
			// inStream.pipe(videoCp.stdin);
			// videoCp.stderr.pipe(process.stderr)
			videoCp.on('exit', code => {
				if (code) {
					return callback(new Error('Transcoding error'));
				}

				childProcess.execFile(path.join(__dirname, '..', 'ffmpeg-linux-64bits', 'ffprobe'), [
					'-print_format',
					'json',
					'-v', 'quiet',
					'-show_streams',
					transcodeFilename
				], (err, stdout, stderr) => {
					if (err) {
						return callback(new Error('FFProbe error'));
					}
					let data = JSON.parse(stdout);
					let audioStream = lodash.find(data.streams, {codec_type: 'audio'});
					let videoStream = lodash.find(data.streams, {codec_type: 'video'});
					let duration = parseFloat(data.streams[0].duration);
					let streams = [
						{
							stream: fs.createReadStream(transcodeFilename),
							mime: mime.lookup('mp4'),
							tag: 'video',
							metadata: {
								duration: duration,
							},
							extension: 'mp4',
						},
					];
					if (videoStream) {
						let thumbCp = childProcess.spawn(path.join(__dirname, '..', 'ffmpeg-linux-64bits', 'ffmpeg'), [
							'-i', transcodeFilename,
							'-vframes', '1',
							'-f', 'mjpeg',
							'-',
						]);
						streams.push({
							stream: thumbCp.stdout,
							mime: 'image/jpeg',
							tag: 'thumb',
							extension: 'jpg',
						});
					}
					callback(null, {
						finish: () => {
							try {
								fs.unlinkSync(inputFilename);
							} catch (e) {
							}
							try {
								fs.unlinkSync(transcodeFilename);
							} catch (e) {
							}
						},
						streams: streams,
					});
				});
			});
		});
};
