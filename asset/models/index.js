"use strict";
module.exports = {
	photo: require('./photo'),
	avatar: require('./avatar'),
	video: require('./video'),
	file: require('./file'),
	_multimediaMap: [
		{mime: 'image/', embedType: 'photo', thumbnailTag: '360', mainTag: '720', config: 'photo',},
		{mime: 'video/|audio/', embedType: 'video', thumbnailTag: 'thumb', mainTag: 'video', config: 'video',},
	],
};
