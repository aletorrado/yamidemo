"use strict";
module.exports = {
	processor: 'image',
	options: [
		{format: 'jpg', quality: 85, interlace: 'Line', type: 'thumbnail', size: [360, 360], tag: 't360'},
		{format: 'jpg', quality: 85, interlace: 'Line', size: [320, 180], tag: '180'},
		{format: 'jpg', quality: 85, interlace: 'Line', size: [640, 360], tag: '360'},
		{format: 'jpg', quality: 85, interlace: 'Line', size: [1280, 720], tag: '720'},
		{format: 'webp', quality: 85, interlace: 'Line', size: [1280, 720], tag: '720'},
	],
};
