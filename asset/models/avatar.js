"use strict";
module.exports = {
	processor: 'image',
	options: [
		{format: 'jpg', quality: 85, interlace: 'Line', type: 'thumbnail', size: [256, 256], tag: 't256'},
		{format: 'jpg', quality: 85, interlace: 'Line', type: 'thumbnail', size: [512, 512], tag: 't512'},
		{format: 'webp', quality: 85, interlace: 'Line', type: 'thumbnail', size: [256, 256], tag: 't256'},
		{format: 'webp', quality: 85, interlace: 'Line', type: 'thumbnail', size: [512, 512], tag: 't512'},
	],
};
