'use strict';
const path = require('path');
const async = require('promise-async');

const run = () => {

	let db = require(__basedir + '/db')();
	let config = require(__basedir + '/config');
	let writers = config.asset.writers
		.filter(x => {
			return x.delete;
		})
		.map(x => {
			return new (require('./writers')[x.type])(x.options);
		});

	return db.Asset.find({
		$or: [
			{
				createdAt: {$lt: new Date(Date.now() - 1000 * 3600)},
				refs: {$size: 0},
				// status: {$ne: 'TRASH'},
			},
		],
	})
		.then(function (assets) {
			return async.eachSeries(assets, function (asset, callback) {
				async.each(asset.files, function (file, callback) {
					if (!file.path) return callback(); // legacy
					async.each(writers, function (writer, callback) {
						if (!writer.deleteFile) {
							return callback();
						}
						writer.deleteFile(path.join(asset.id, file.path))
							.then(() => {
								callback();
							})
							.catch(err => {
								callback();
							})
					}, callback);
				})
					.then(function () {
						async.each(writers, function (writer, callback) {
							let p;
							if (writer.deleteFolder) {
								p = writer.deleteFolder(asset.id);
							} else {
								p = Promise.resolve();
							}
							p
								.then(function () {
									// asset.status = 'TRASH';
									// asset.save(callback);
									asset.remove(callback);
								})
								.catch(err => {
									callback();
								})
						}, callback)
					})
					.catch(callback);
			});
		});
};

module.exports.run = run;

if (!module.parent) {
	global.__basedir = path.join(__dirname, '..');
	run()
		.then(function () {
			process.exit(0);
		})
		.catch(function (err) {
			throw err;
		});
}
