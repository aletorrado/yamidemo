'use strict';

module.exports = {
	local: require('./local'),
	filesystem: require('./filesystem'),
	googlecloud: require('./googlecloud'),
};
