'use strict';
var path = require('path');
var lodash = require('lodash');

class GoogleCloudAdapter {
	constructor(options){
		this.options = lodash.defaults(options || {}, {
			// key: 'googlecloud.json',
		});
		this.gcs = require('@google-cloud/storage')({
			projectId: this.options.project,
			keyFilename: this.options.key && path.join(__basedir, this.options.key),
		});
		this.bucket = this.gcs.bucket(this.options.bucket);
	}
	put(filename, mime){
		var opts = {
			metadata: {
				cacheControl: "public, max-age=31536000",
				contentType: mime,
			},
		};
		return Promise.resolve({
			stream: this.bucket.file(filename).createWriteStream(opts),
		});
	}
	deleteFile(filename){
		return this.bucket.file(filename).delete();
	}
}

module.exports = GoogleCloudAdapter;
