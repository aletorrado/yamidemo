'use strict';

var path = require('path');
var fs = require('fs');

module.exports = function(opts) {
	return {
		url: function(filename){
			return path.join('/assets/', filename);
		},
		put: function(filename) {
			return fs.createWriteStream(__basedir + '/public/assets/' + filename);
		},
		delete: function(filename, callback) {
			fs.unlink(__basedir + '/public/assets/' + filename, callback);
		}
	}
};
