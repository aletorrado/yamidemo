'use strict';
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var rimraf = require('rimraf');

class FilesystemAdapter {
	constructor(options){
		this.options = options;
	}
	put(filename, mime) {
		return new Promise(function(resolve, reject){
			var id = /[0-f]{24}/.exec(filename)[0];
			var target = path.parse(filename);
			mkdirp(path.join(__basedir, this.options.directory, target.dir), function(err){
				if (err) return reject(err);
				resolve({
					stream: fs.createWriteStream(path.join(__basedir, this.options.directory, filename)),
				})
			}.bind(this));
		}.bind(this))
	}
	deleteFolder(filename){
		return new Promise(function(resolve, reject){
			var id = /[0-f]{24}/.exec(filename)[0];
			rimraf(path.join(__basedir, this.options.directory, filename), function(err){
				if (err) return reject(err);
				resolve(); // always resolve
			});
		}.bind(this))
	}
}

module.exports = FilesystemAdapter;
