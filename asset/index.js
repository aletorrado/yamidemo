'use strict';

module.exports = {
	processors: require('./processors'),
	writers: require('./writers'),
	mongoose: require('./mongoose'),
	express: require('./express'),
	trasher: require('./trasher'),
	insert: require('./insert'),
};
