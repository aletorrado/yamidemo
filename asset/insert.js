'use strict';
const async = require('async');
const lodash = require('lodash');
const path = require('path');
const mongoose = require('mongoose');
const stream = require('stream');
const streamMeter = require('stream-meter');
const promiseAsync = require('promise-async');
const bluebird = require('bluebird');
const config = require('config');

const processors = require('./processors');
const assetConfigs = require('./models');

const writers = config.get('asset.writers').map(function (x) {
	return new (require('./writers')[x.type])(x.options);
});

const semaphore = new (require('await-semaphore').Semaphore)(4);

const getProcessor = (name) => {
	let processor = processors[name];
	return bluebird.promisify(processor);
};

const insert = (model, file, filename, mime, assetConfig) => {
	let def = assetConfigs[model];
	if (!def) reject(new Error("Model not defined."));
	let processor = getProcessor(def.processor);
	let oid, id;
	do {
		oid = mongoose.Types.ObjectId();
		id = oid.toString();
	} while (id.substr(-2) === 'ad'); // adblock false positive prevention
	return semaphore.use(() => {
		return processor({
			filename: filename,
			file: file,
			mime: mime,
			options: def.options,
		})
			.then((processorOutput) => {
				if (Array.isArray(processorOutput)) {
					processorOutput = {streams: processorOutput};
				}
				let outStreams = processorOutput.streams;
				return mongoose.model('Asset').create({
					_id: oid,
					model: model,
					processor: def.processor,
					metadata: processorOutput.metadata,
					files: lodash.map(outStreams, function (x) {
						x.path = x.path || lodash.compact([x.tag || 'index', x.extension]).join(".");
						return {
							path: x.path,
							mime: x.mime,
							tag: x.tag,
							metadata: x.metadata,
						}
					}),
				})
					.then((assetDoc) => {
						return promiseAsync.map(outStreams, function (outStream, callback) {
							let mMeter = new streamMeter();
							let dataStream = outStream.stream
								.pipe(mMeter);
							async.each(writers, function (writer, callback) {
								let pt = new stream.PassThrough();
								dataStream.pipe(pt);
								writer.put(path.join(id, outStream.path), outStream.mime)
									.then(function (writerData) {
										let mMeter = new streamMeter();
										pt
											.pipe(writerData.stream)
											.on('error', callback)
											.on('finish', callback)
									})
									.catch(callback)
							}, function (err) {
								if (err) return callback(err);
								callback(null, {
									size: mMeter.bytes,
								});
							})
						})
							.catch(async err => {
								processorOutput.finish && processorOutput.finish();
								assetDoc.status = 'ERROR';
								await assetDoc.save();
								throw err;
							})
							.then(async (filesData) => {
								processorOutput.finish && processorOutput.finish();
								assetDoc.files.forEach(function (fileInfo, idx) {
									lodash.assign(fileInfo, filesData[idx]);
								});
								assetDoc.status = 'OK';
								await assetDoc.save();
								let data = lodash.pick(assetDoc, [
									'id',
									'files',
								]);
								data.files = lodash.map(data.files, function (x) {
									return {
										mime: x.mime,
										url: x.url,
										path: x.path,
										tag: x.tag,
										metadata: x.metadata,
										size: x.size,
									}
								});
								return data;
							});
					});
			});
	});
};

module.exports = insert;
