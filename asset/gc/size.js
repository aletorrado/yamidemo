"use strict";
const path = require('path');
const filesize = require('filesize');

global.__basedir = path.join(__dirname, '..', '..');
const context = {};
const db = context.db = require(__basedir + '/db')(context);

let purgeableConds = {
	createdAt: {$lt: new Date(new Date().getTime() - 1000 * 3600)},
	refs: {$size: 0},
};

db.Asset.aggregate([
	{$match: purgeableConds},
	{$sort: {date: 1}},
	{$unwind: '$files'},
	{$group: {_id: null, sum: {$sum: '$files.size'}}}
])
	.exec((err, data) => {
		if (err) {
			throw err;
		}
		console.log(filesize(data[0].sum));
		process.exit();
	});
