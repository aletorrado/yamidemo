"use strict";
const path = require('path');
const filesize = require('filesize');

global.__basedir = path.join(__dirname, '..', '..');
const context = {};
const db = context.db = require(__basedir + '/db')(context);

let purgeableConds = {
	createdAt: {$lt: new Date(new Date().getTime() - 1000 * 3600)},
	refs: {$size: 0},
};

db.Asset.aggregate([
	{$match: purgeableConds},
	{$sort: {date: 1}},
	{$unwind: '$files'},
])
	.exec((err, data) => {
		if (err) {
			throw err;
		}
		data.forEach(function (item) {
			console.log(item.createdAt.toISOString() + ' - ' + item.files.privateUrl + ' - ' + (item.files.size && filesize(item.files.size) || 0));
		});
		process.exit();
	});
