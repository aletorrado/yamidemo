'use strict';
const lodash = require('lodash');
const mpath = require('mpath');
const mongoose = require('mongoose');

module.exports = function (schema, options) {
	let paths = [];

	const process = (obj, parent) => {
		if (!parent) {
			parent = [];
		}
		for (let fn in obj) {
			if (obj[fn] instanceof mongoose.Schema.Types.ObjectId && obj[fn].options.ref === 'Asset' || (obj[fn].options.type[0] && obj[fn].options.type[0].ref) === 'Asset') {
				paths.push(parent.concat([fn]).join('.'));
			} else if (obj[fn] instanceof mongoose.Schema.Types.DocumentArray) {
				process(obj[fn].schema.paths, parent.concat([fn]));
			}
		}
	};

	process(schema.paths);

	schema.pre('remove', function (next) {
		let currentDoc = this;
		let model = currentDoc.constructor.modelName;
		let docId = currentDoc._id;
		mongoose.model('Asset')
			.update({
				refs: {$elemMatch: {model: model, docId: docId}},
			}, {
				$pull: {
					refs: {$elemMatch: {model: model, docId: docId}},
				},
			}, {
				multi: true,
			}, next);
	});

	schema.pre('save', function (next) {
		let currentDoc = this;
		let model = currentDoc.constructor.modelName;
		let docId = currentDoc._id;

		let ids = lodash.chain(paths).map(function (field) {
			let ids = mpath.get(field, currentDoc);
			if (!Array.isArray(ids)) {
				ids = [ids];
			}
			return lodash.chain(ids).flatten().value();
		}).flatten().compact().uniqBy((x) => {
			return x.toString();
		}).value();

		mongoose.model('Asset').update({
			refs: {$elemMatch: {model: model, docId: docId}},
		}, {
			$pull: {
				refs: {$elemMatch: {model: model, docId: docId}},
			},
		}, {
			multi: true,
		}, (err) => {
			if (err) {
				return next(err);
			}
			mongoose.model('Asset').update({
				_id: {$in: ids},
				refs: {$not: {$elemMatch: {model: model, docId: docId}}},
			}, {
				$addToSet: {
					refs: {model: model, docId: docId},
				},
			}, {
				multi: true,
			}, next);
		});
	});
};
