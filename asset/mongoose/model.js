'use strict';
const url = require('url');
const path = require('path');
const config = require('config');

const foreignKey = require(__basedir + '/plugins/foreign_key');

module.exports = (mongoose) => {
	let mySchema = mongoose.Schema({
		_id: {type: mongoose.Schema.Types.ObjectId, auto: false, required: true},
		status: {type: String, required: true, default: 'PROCESSING', enum: ['PROCESSING', 'OK', 'ERROR', 'TRASH']},
		model: String,
		processor: String,
		files: [{
			url: {
				type: String, get: function (x) {
					let mUrl;
					if (this.publicUrl) {
						mUrl = this.publicUrl;
						/* deprecated */
					} else if (this.path) {
						mUrl = path.join(this.ownerDocument().id, this.path);
					} else {
						throw new Error('No clue where is it.');
					}
					if (config.get('asset.endpoint')) {
						mUrl = url.resolve(config.get('asset.endpoint') + '/', mUrl);
					}
					return mUrl;
				}
			},
			path: {type: String},
			publicUrl: String, /* deprecated */
			size: Number,
			mime: String,
			tag: String,
			metadata: mongoose.Schema.Types.Mixed,
			_id: false,
		}],
		metadata: mongoose.Schema.Types.Mixed,
		refs: [{
			model: {type: String, required: true},
			docId: {type: mongoose.Schema.Types.ObjectId, required: true},
			_id: false,
		}],
		updatedAt: Date,
		createdAt: Date,
	})
		.plugin(foreignKey, {
			models: [
				{model: 'User', field: 'photo'},
			]
		})
		.pre('save', function (next) {
			let d = new Date();
			if (this.isModified()) {
				this.updatedAt = d;
			}
			if (this.isNew) {
				this.createdAt = d;
			}
			next();
		})
		.index({'refs.model': 1, 'refs.docId': 1})
		.plugin(require('mongoose-deep-populate')(mongoose));

	return mongoose.model('Asset', mySchema);
};
