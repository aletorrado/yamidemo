'use strict';

module.exports = {
	model: require('./model'),
	plugin: require('./plugin'),
};
