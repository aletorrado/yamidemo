"use strict";
const lodash = require('lodash');
const express = require('express');

const assetConfigs = require('../models');
const insert = require('../insert');

let myRouter = express.Router();

myRouter.post('/multimedia', (req, res, next) => {
	let maps = assetConfigs._multimediaMap;
	req.assetConfig = lodash.chain(maps).filter(function (x) {
		return new RegExp(x.mime).test(req.headers['content-type']);
	}).first().value();
	req.url = '/' + (req.assetConfig && req.assetConfig.config);
	next();
});

myRouter.post('/:type', (req, res, next) => {
	req.setTimeout(100000); // allow heavy processings
	let defId = req.params.type;
	let def = assetConfigs[defId];
	if (!def) return res.sendStatus(400);
	// var processor = processors[def.processor];
	// var file = req;
	let mimetype = req.headers['content-type'];
	let filename = lodash.chain(req.headers['content-disposition'])
		.split(/; */)
		.filter((x) => {
			return /^filename=/.test(x)
		})
		.map((x) => {
			return x.split('"')[1];
		})
		.first()
		.value();
	insert(defId, req, filename, mimetype, req.assetConfig)
		.then((data) => {
			res.send(data);
		})
		.catch(next);
});

module.exports = myRouter;
