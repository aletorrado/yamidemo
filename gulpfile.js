'use strict';
const gulp = require('gulp');
const path = require('path');
const fs = require('fs');
const child_process = require('child_process');

require('./web/gulpfile')(gulp);

gulp.task('version', (callback) => {
	child_process.exec("git log --pretty=format:'%d %H' -n 1", (err, stdout, stderr) => {
		if (err) {
			return callback(err);
		}
		fs.writeFile(path.join(__dirname, 'version.txt'), stdout, callback);
	});
});

gulp.task('default', gulp.series([
	'version',
	'web'
]));
