'use strict';
const lodash = require('lodash');
const async = require('async');
const mongoose = require('mongoose');

const queryUtils = require(__basedir + '/utils/query-utils');

module.exports = function (schema, options) {
	schema.pre('remove', function (next) {
		let self = this;
		async.map(options.models, (model, callback) => {
			let query = {};
			if (model.fields || options.fields) {
				query.$or = [];
				(model.fields || options.fields).forEach((key) => {
					let condition = {};
					condition[key] = self;
					query.$or.push(condition);
				});
			} else {
				let key = model.field || options.field;
				query[key] = self;
			}
			mongoose.model(model.model || model).countDocuments(query).exec(callback);
		}, (err, counts) => {
			if (err) {
				return next(err);
			}

			if (lodash.sum(counts)) {
				return next(new Error(queryUtils.ERRORS.FOREIGN_KEY));
			}
			next();
		});
	});
};
