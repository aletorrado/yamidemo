'use strict';
const mongoose = require('mongoose');
const lodash = require('lodash');

const asset = require(__basedir + '/asset');

module.exports = (schema, opts) => {
	schema.plugin(require('mongoose-deep-populate')(mongoose));
	schema.plugin(asset.mongoose.plugin);
	opts = lodash.defaults(opts, {
		updatedAt: true,
		createdAt: true,
		createdBy: true,
	});
	if (opts.createdAt) {
		schema.add({
			createdAt: Date,
		});
		schema.index({createdAt: -1});
	}
	if (opts.updatedAt) {
		schema.add({
			updatedAt: Date,
		});
		schema.index({updatedAt: -1});
	}
	if (opts.createdBy) {
		schema.add({
			createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
		});
		schema.index({createdBy: 1});
	}
	schema.pre('save', function(next){
		var now = new Date();
		if (this.isModified()) {
			this.updatedAt = now;
		}
		if (this.isNew) {
			this.createdAt = now;
		}
		next();
	});
};
