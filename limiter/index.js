"use strict";

class Limiter {
	constructor(windowMs, maxAmount) {
		this.windowMs = windowMs;
		this.maxAmount = maxAmount;
		this.slidingWindow = [];
	}

	take(n) {
		return new Promise((resolve, reject) => {
			n = n || 1;
			this.slidingWindow = this.slidingWindow.filter(function (x) {
				return x > (Date.now() - this.windowMs);
			}.bind(this));
			if (this.slidingWindow.length + n <= this.maxAmount) {
				for (let i = 0; i < n; i++) this.slidingWindow.push(new Date());
				resolve({
					go: true,
				});
			} else {
				let resumeAtForN = this.slidingWindow[n - 1];
				if (resumeAtForN) {
					resumeAtForN = new Date(resumeAtForN.getTime() + this.windowMs);
				}
				resolve({
					go: false,
					resumeAt: resumeAtForN,
				});
			}
		})
	}
}

class KeyHashLimiter {
	constructor(windowMs, maxAmount, hashSize) {
		this.limiters = new Array(hashSize);
		this.windowMs = windowMs;
		this.maxAmount = maxAmount;
		this.hashSize = hashSize;
	}

	hash(key) {
		let hash = 0;
		if (!key.length) {
			return hash;
		}
		let char;
		for (let i = 0; i < key.length; i++) {
			char = key.charCodeAt(i);
			hash = ((hash << 5) - hash) + char;
		}
		return hash;
	}

	take(key, n) {
		let hash = this.hash(key) % this.hashSize;
		if (!this.limiters[hash]) {
			this.limiters[hash] = new Limiter(this.windowMs, this.maxAmount);
		}
		return this.limiters[hash].take(n);
	}
}

const expressIpLimiter = (windowMs, maxAmount, hashSize) => {
	let limiter = new KeyHashLimiter(windowMs, maxAmount, hashSize || (256 * 256));
	return function (req, res, next) {
		limiter.take(req.ip || '')
			.then(limiterData => {
				if (limiterData.go) {
					next();
				} else {
					res.sendStatus(429);
				}
			})
	}
};

module.exports.Limiter = Limiter;
module.exports.KeyHashLimiter = KeyHashLimiter;
module.exports.expressIpLimiter = expressIpLimiter;