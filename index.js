'use strict';
const express = require('express');
const domain = require('domain');
const fs = require('fs');
const config = require('config');
const lodash = require('lodash');
const subdomain = require('express-subdomain');

global.__basedir = __dirname;

const monitoring = require(__basedir + '/monitoring')();
const db = require(__basedir + '/db');

let LISTEN = config.get('listen');
if (isNaN(LISTEN)) {
	LISTEN = __dirname + '/' + LISTEN;
	try {
		fs.unlinkSync(LISTEN);
	} catch(e) {}
}

let development = process.env.NODE_ENV === 'development';

if (development || process.env.NODE_APP_INSTANCE === 0) {
	console.log('Starting scheduler...');
	require('./scheduler')(db);
}

let app = express();

if (!development) {
	app.use(monitoring.express());
	monitoring.mongoose(db.mongoose);
}

app.use((req, res, next) => {
	req.domain = domain.create();
	req.domain.on('error', next);
	req.domain.run(next);
});

app.use(require('body-parser').json());
app.use(require('body-parser').urlencoded({extended: true}));

app.disable('x-powered-by');
app.enable('trust proxy');

app.use(express.static(__dirname + '/public'));

app.set('subdomain offset', config.get('host.name').split('.').length);

let context = {
	app: app,
	db: db
};
lodash.forEach(config.get('services'), (service) => {
	if (service.subdomain) {
		app.use(subdomain(service.subdomain, require('./' + service.path)(context)));
	} else {
		require('./' + service.path)(context);
	}
});

app.use((err, req, res, next) => {
	console.error(new Date(), err && err.stack || err);
	err.errors && console.error(err.errors);
	res.sendStatus(err.statusCode || err.status || 500);
});

app.listen(LISTEN, (err) => {
	if (err) {
		throw err;
	}
	if (isNaN(LISTEN)) {
		fs.chmodSync(LISTEN, 666);
	}

	console.log('Server now listening: ' + LISTEN);
});

process.on('uncaughtException', (err) => {
	console.error(new Date(), 'Uncaught Exception', err);
});
