'use strict';
const fs = require('fs');
const config = require('config');
const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-transport-sendgrid');
const handlebars = require('handlebars');
const sprintf = require('sprintf-js').sprintf;

const template = handlebars.compile(fs.readFileSync(__dirname + '/template.html').toString());

const transporter = nodemailer.createTransport(sgTransport(config.get('mailer.apiKey')));

module.exports = {
	send: (opts, callback) => {
		let data = { //IMPORTANT outside saveEmail because can be updated
			to: opts.to,
			subject: opts.subject,
			message: opts.html
		};
		if (opts.attachments && opts.attachments.length) {
			data.attachments = opts.attachments.length;
		}

		if (opts.dummy) {
			return callback && callback();
		}

		if (!process.env.NODE_ENV === 'production' && !opts.monitoring) {
			opts.subject += ' [' + opts.to + ']';
			opts.to = "yamilafraiman@gmail.com";
		}

        opts.from = sprintf('%s <%s>', process.env.APP_NAME, opts.from);
		if (opts.template) {
            opts.subject = process.env.APP_NAME + ' - ' + opts.subject;
            opts.html = template({
                html: opts.html
            });
            transporter.sendMail(opts, callback);
		} else {
			transporter.sendMail(opts, callback);
		}
	}
};
