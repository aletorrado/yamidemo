'use strict';
const express = require('express');
const config = require('config');
const morgan = require('morgan');
const passport = new (require('passport')).Passport(); // dont use the singleton passport for security

const asset = require(__basedir + '/asset');

module.exports = (context) => {

	const router = express.Router();

	if (config.get('debug')) {
		router.use(morgan('Api - :method :url :status - [:date[clf]] - :remote-addr - :response-time'));
	}

	const db = context.db;

	router.use('/session', require('./auth')(db, passport));
	router.use('/asset', asset.express.middleware);
	router.use('/account', require('./account')(db));

	router.use((req, res, next) => {
		passport.authenticate(['bearer'], (err, user) => {
			if (err) {
				return next(err);
			}

			if (!user) {
				return res.sendStatus(401);
			}

			req.user = user;
			next();
		})(req, res, next);
	});

	router.use(require(__basedir + '/utils/mongoose-router')(db, false, require('./models'), {
		// safe defaults for public access
		modelHandlers: {
			GET: true,
		},
		documentHandlers: {
			GET: true,
		},
	}));

	// Additional security measure: unauthenticated users can only GET
	router.use((req, res, next) => {
		if (req.user || req.method === 'GET') {
			next();
		} else {
			res.sendStatus(401);
		}
	});

	router.use('/user', require('./user')(db));

	context.app.use('/api/v1', router);

};
