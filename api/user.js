'use strict';
const async = require('async');
const lodash = require('lodash');
const express = require('express');

const queryUtils = require(__basedir + '/utils/query-utils');

module.exports = (db) => {
	const router = express.Router();

	const fields = [
		'firstName',
		'lastName',
		'displayName',
		'photo',
	];
	const excludeFields = ['password'];
	const editableFields = lodash.union(fields, ['password']);
	const viewableFields = lodash.chain(fields).union(['email']).difference(excludeFields).value();
	const populableFields = ['photo'];

	router.get('/', (req, res, next) => {
		req.user.populate(populableFields, (err, user) => {
			if (err) {
				return next(err);
			}

			let data = queryUtils.serialize(user, db, 'User', viewableFields);
			res.send({
				profile: data,
			});
		});
	});

	router.post('/', (req, res, next) => {
		let data = lodash.pick(req.body, editableFields);
		data = queryUtils.fixData(data);
		lodash.assign(req.user, data);
		req.user.save((err, doc) => {
			if (err) {
				if (err.code === queryUtils.DUPLICATED_CODE) {
					return res.send({
						error: queryUtils.ERRORS.DUPLICATED
					});
				}

				return next(err);
			}

			data = queryUtils.serialize(doc, db, 'User', viewableFields);
			res.send(data);
		});
	});

	router.post('/password', (req, res, next) => {
		if (!req.body.password) {
			return res.sendStatus(400);
		}

		req.user.password = req.body.password;
		req.user.auth = lodash.filter(req.user.auth, {accessToken: req.headers['access-token']});

		req.user.save((err) => {
			if (err) {
				return next(err);
			}

			res.send();
		});
	});

	return router;

};
