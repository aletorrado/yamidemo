'use strict';
const express = require('express');
const passportBearer = require('passport-http-bearer');
const passportLocal = require('passport-local');
const config = require('config');

const limiter = require(__basedir + '/limiter');

module.exports = (db, passport) => {

	passport
		.use(new passportBearer(
			(token, callback) => {
				db.User
					.findOneAndUpdate({
						'auth.accessToken': token,
						disabled: false,
					}, {
						$set: {
							'auth.$.lastAccessAt': new Date(),
						},
					}, { new: true })
					.exec(callback);
			}
		))
		.use(new passportLocal.Strategy((username, password, callback) => {
			db.User.findOne({
				email: username,
				disabled: false,
			}, (err, doc) => {
				if (err || !doc) {
					return callback(err);
				}

				doc.comparePassword(password, (err, isMatch) => {
					if (err || !isMatch) {
						return callback(err);
					}

					callback(null, doc);
				});
			});
		}));

	const router = express.Router();

	process.env.NODE_ENV === 'development' || router.use(limiter.expressIpLimiter(1000 * 60 * 2, 50));

	router.use(passport.authenticate([
		'bearer',
		'local',
	], { session: false }));

	router.post('/', (req, res, next) => {
		req.user.addAccessToken((err, data) => {
			if (err) {
				return next(err);
			}

			req.user.save((err) => {
				if (err) {
					return next(err);
				}

				res.send({
					accessToken: data.accessToken,
				});
			});
		});
	});

	router.delete('/', (req, res, next) => {
		req.user.removeAccessToken(req.authInfo, (err) => {
			if (err) {
				return next(err);
			}

			req.user.save((err) => {
				if (err) {
					return next(err);
				}

				res.send();
			});
		});
	});

	return router;

};
