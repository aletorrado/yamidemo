'use strict';
const lodash = require('lodash');

module.exports = {
	id: 'User',
	endpoint: null,
	picker: (doc, req) => {
		let viewableFields = ['id', 'displayName', 'photo'];
		if (doc.id === req.user.id) {
			return lodash.pick(doc, lodash.union(['name', 'email'], viewableFields));
		}
		return lodash.pick(doc, viewableFields);
	},
};
