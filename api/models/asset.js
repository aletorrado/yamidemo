'use strict';

module.exports = {
	id: 'Asset',
	picker: (doc, req, viewableFields, serialize) => {
		return {
			id: doc.id,
			status: doc.status,
			metadata: serialize(doc.metadata),
			files: doc.files.map(file => {
				return {
					url: file.url,
					size: file.size,
					mime: file.mime,
					tag: file.tag,
					metadata: file.metadata,
				};
			}),
		};
	},
};
