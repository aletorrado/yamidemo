'use strict';
const glob = require('glob');

module.exports = glob.sync('*', {cwd: __dirname})
	.filter(filename => filename !== 'index.js' && filename[0] !== '_')
	.map(filename => require(`./${filename}`));
