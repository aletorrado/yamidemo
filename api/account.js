'use strict';
const sprintf = require('sprintf-js').sprintf;
const express = require('express');
const lodash = require('lodash');
const config = require('config');

const mailer = require(__basedir + '/mailer');
const queryUtils = require(__basedir + '/utils/query-utils');

module.exports = (db) => {

	const router = express.Router();

	const sendPasswordEmail = (req, user, callback) => {
		user.resetPasswordToken((err, token) => {
			if (err) {
				return callback(err);
			}

			let link = sprintf('%s://%s/%s', config.get('host.protocol'), config.get('host.name'), 'password-recovery/?token=' + token);
			let html;
			if (user.password) {
				html = '<p>Se ha solicitado recuperar la contraseña.</p>';
			} else {
				html = sprintf('<p>Gracias por registrarse en %s.</p>', config.get('name'));
			}
			html += '<p>Por favor, haga click en el siguiente link o cópielo y péguelo en el navegador para completar el proceso.</p>' +
				'<p><a clicktracking="off" href="' + link + '">' + link + '</a></p>' +
				'<p>Si considera que ha ocurrido un error, ignore este email.</p>';
			let message = {
				to: user.email,
				html: html,
				subject: user.password ? 'Recuperar contraseña' : 'Confirmación email',
				template: true,
			};
			mailer.send(message, (err) => {
				if (err) {
					console.error('Password email failed.', err); //Users never realise that the account has been created
				}

				callback(); //Do not fail is email wasn't sent
			});
		});
	};

	router.post('/', (req, res, next) => {
		let email = req.body.email;
		if (!email) {
			return res.sendStatus(400);
		}

		db.User.findOne({email: email}).exec((err, user) => {
			if (err) {
				return next(err);
			}

			if (user) {
				return res.send({
					error: queryUtils.ERRORS.DUPLICATED_EMAIL
				});
			}

			let data = lodash.pick(req.body, ['name', 'email']);
			data = queryUtils.fixData(data);
			user = new db.User(data);

			sendPasswordEmail(req, user, (err) => { //This methods will save user
				if (err) {
					return next(err);
				}

				res.send();
			});
		});
	});

	router.post('/recover', (req, res, next) => {
		let email = req.body.username;
		if (!email) {
			return res.sendStatus(400);
		}

		db.User.findOne({email: email}).exec((err, user) => {
			if (err) {
				return next(err);
			}
			if (!user) {
				return res.sendStatus(404);
			}

			if (user.disabled) {
				return res.send({
					error: queryUtils.ERRORS.DISABLED
				});
			}

			//FIXME max 3 retries if no password
			if (user.password && user.resetPassword && user.resetPassword.expiresAt && user.resetPassword.expiresAt.getTime() > new Date().getTime()) {
				return res.send({
					error: queryUtils.ERRORS.PENDING_RECOVERY
				});
			}

			sendPasswordEmail(req, user, (err) => {
				if (err) {
					return next(err);
				}

				const censorWord = (str) => {
					if (str.length <= 2) {
						return '*'.repeat(str.length);
					}
					return str[0] + '*'.repeat(str.length - 2) + str.slice(-1);
				};

				let arr = user.email.split('@');
				res.send({
					email: censorWord(arr[0]) + '@' + arr[1]
				});
			});
		});
	});

	router.post('/recover/:token', (req, res, next) => {
		if (!req.params.token || !req.body.password) {
			return res.sendStatus(400);
		}

		db.User.findOne({
			'resetPassword.token': req.params.token,
			'resetPassword.expiresAt': {$gt: Date.now()},
			'disabled': false
		}).exec((err, user) => {
			if (err) {
				return next(err);
			}
			if (!user) {
				return res.sendStatus(404);
			}

			user.password = req.body.password;
			user.resetPassword = undefined;

			user.auth = [];

			user.save((err) => {
				if (err) {
					return next(err);
				}

				res.send();
			});
		});
	});

	return router;

};
