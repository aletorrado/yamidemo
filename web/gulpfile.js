'use strict';
const path = require('path');
const fs = require('fs');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config');

module.exports = (gulp) => {

	gulp.task('web/webpack', async () => {
		return gulp.src([
			'web/src/index.js',
		])
			.pipe(webpackStream(webpackConfig(), null, (err, stats) => {
				if (err) {
					throw err;
				}
				fs.writeFileSync(path.join(__dirname, 'webpack-stats.json'), JSON.stringify(stats.toJson()));
			}))
			.pipe(gulp.dest('web/dist'));
	});

	gulp.task('web', gulp.series(
		'web/webpack'
	));

};
