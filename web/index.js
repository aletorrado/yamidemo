'use strict';
const morgan = require('morgan');
const config = require('config');
const webpack = require('webpack');
const express = require('express');
const path = require('path');
const webpackDevMiddleware = require('webpack-dev-middleware');

const webpackConfig = require('./webpack.config');

module.exports = (context) => {

	if (config.get('debug')) {
		context.app.use(morgan('Web - :method :url :status - [:date[clf]] - :remote-addr - :response-time'));
	}

	if (process.env.NODE_ENV === 'development') {
		context.app
			.use(webpackDevMiddleware(webpack(webpackConfig(true)), {
				logLevel: 'error'
			}));
	} else {
		context.app.use(express.static(path.join(__dirname, 'dist')));
	}

	context.db.User.estimatedDocumentCount((err, count) => {
		if (!count) {
			context.db.User.create({
				firstName: 'system',
				lastName: 'admin',
				email: 'admin@99uno.com',
				password: 'asdasd',
				systemAdmin: true,
			}, (err) => {
				if (err) {
					return console.error('Error creating web user: ' + err);
				}

				console.log('web user created.');
			});
		}
	});

};
