'use strict';
const handlebars = require('handlebars');
const engine = handlebars.create();

const escapeHtml = (string) => {
	return String(string).replace(/[&<>"'\/]/g, (s) => {
		return {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			'"': '&quot;',
			"'": '&#39;',
			"/": '&#x2F;'
		}[s];
	});
};

const escapeCss = (str) => {
	let chars = ['(', ')', ';', "'", '"'];
	chars.forEach((char) => {
		str = str.replace(new RegExp("\\"+char, "g"), "\\"+char);
	});
	return str;
};

engine.registerHelper('br', (text) => {
	let html = escapeHtml(text)
		.replace(/ /g, '&nbsp;')
		.replace(/\n/g, '<br />')
		.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
	return new handlebars.SafeString(html);
});

module.exports = engine;
