"use strict";

module.exports = (context) => {

	router.get('/instance', (req, res) => {
		res
			.type('text/plain')
			.send([
				'HOSTNAME: ' + require('os').hostname(),
				'PID: ' + process.pid,
				'CLIENT IP: ' + req.ip,
				'DEBUG: ' + config.get('debug')
			].join('\n'));
	});

	router.get('/health', (req, res, next) => {
		context.db.Settings.findOne((err) => {
			if (err) {
				return next(err);
			}
			res.type('text/plain').send('OK');
		});
	});

	router.get('/robots.txt', (req, res, next) => {
		let props = [
			['User-agent', '*'],
			['Disallow', '/assets']
		];
		res.type('text/plain')
			.send(lodash.map(props, (x) => {
				return x[0] + ': ' + x[1];
			}).join('\n'));
	});

	require('./errors')(context); // IMPORTANT last import
};
