"use strict";
const fs = require('fs');

const handlebars = require('./handlebars');

const notFoundTemplate = handlebars.compile(fs.readFileSync(__dirname + '/html/404.html').toString());
const errorTemplate = handlebars.compile(fs.readFileSync(__dirname + '/html/500.html').toString());

module.exports = (router, db) => {

	router.use((req, res, next) => {
		res.status(404).send(notFoundTemplate());
	});

	router.use((err, req, res, next) => {
		res.error = err;
		console.error(err && err.stack || err);
		res.status(500).send(errorTemplate({
			stack: err.stack
		}));
	});

};
