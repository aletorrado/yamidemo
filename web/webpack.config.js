'use strict';
const path = require('path');
const lodash = require('lodash');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

module.exports = (development) => {

	const maxSize = 1000 * 1000;

	const srcPath = path.resolve(__dirname, 'src');
	const outputPath = path.resolve(__dirname, 'dist');
	const assetsPath = './assets';
	const cssPath = `${assetsPath}/css`;
	const imagesPath = `${assetsPath}/images`;
	const fontsPath = `${assetsPath}/fonts`;

	return {
		mode: development ? 'development' : 'production',
		resolve: {
			extensions: ['.ts', '.tsx', '.js', '.json'],
		},
		entry: {
			main: ['@babel/polyfill', `${srcPath}/index.js`],
		},
		context: srcPath,
		devtool: development ? 'source-map' : false,
		optimization: {
			minimize: !development,
			minimizer: [
				new TerserPlugin(),
			],
		},
		plugins: lodash.compact([
			development ? new webpack.ProgressPlugin() : null,
			development ? new webpack.HotModuleReplacementPlugin() : null,
			development ? null : new CleanWebpackPlugin(),
			new LodashModuleReplacementPlugin,
			new HtmlWebpackPlugin({
				template: `${srcPath}/index.html`,
			}),
			new MiniCssExtractPlugin({
				filename: `${cssPath}/[name].css`,
				chunkFilename: `${cssPath}/[id].css`,
			}),
		]),
		output: {
			filename: '[name].js',
			path: outputPath,
			publicPath: '/',
		},
		performance: {
			maxAssetSize: maxSize,
			maxEntrypointSize: maxSize,
		},
		module: {
			rules: [
				{
					test: /\.(html)$/,
					use: 'html-loader',
				},
				{
					test: /\.(ts|js)x?$/,
					exclude: [/node_modules/],
					use: {
						loader: 'babel-loader',
						options: {
							presets: [
								'@babel/preset-env',
								'@babel/preset-react',
								'@babel/preset-typescript',
							],
							plugins: [
								'@babel/plugin-proposal-class-properties',
								['lodash', { id: ['lodash', 'recompose'] }],
							]
						},
					},
				},
				{
					test: /\.css$/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
					],
				},
				{
					test: /\.(png|jpg|gif|svg)$/,
					loader: 'file-loader',
					options: {
						name: `[name]` + (development ? '.[ext]' : '.[hash:8].[ext]'),
						outputPath: imagesPath,
					},
				},
				{
					test: /\.(ttf|eot|woff|woff2)$/,
					loader: 'file-loader',
					options: {
						name: `[name].[ext]`,
						outputPath: fontsPath,
					},
				},
			],
		},
	};
};
