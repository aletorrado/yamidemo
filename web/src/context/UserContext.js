import React from "react";
import ApiClient from '../api/api-client';
import { toast } from 'react-toastify';

const UserStateContext = React.createContext();
const UserDispatchContext = React.createContext();

const userReducer = (state, action) => {
	switch (action.type) {
		case 'LOGIN_SUCCESS':
			return { ...state, isAuthenticated: true };
		case 'SIGN_OUT_SUCCESS':
			return { ...state, isAuthenticated: false };
		default: {
			throw new Error(`Unhandled action type: ${action.type}`);
		}
	}
};

const UserProvider = ({ children }) => {
	let [state, dispatch] = React.useReducer(userReducer, {
		isAuthenticated: !!localStorage.getItem('accessToken'),
	});

	return (
		<UserStateContext.Provider value={state}>
			<UserDispatchContext.Provider value={dispatch}>
				{children}
			</UserDispatchContext.Provider>
		</UserStateContext.Provider>
	);
};

const useUserState = () => {
	let context = React.useContext(UserStateContext);
	if (context === undefined) {
		throw new Error('useUserState must be used within a UserProvider');
	}
	return context;
};

const useUserDispatch = () => {
	let context = React.useContext(UserDispatchContext);
	if (context === undefined) {
		throw new Error('useUserDispatch must be used within a UserProvider');
	}
	return context;
};

export { UserProvider, useUserState, useUserDispatch, loginUser, recoverPassword, signOut };

// ###########################################################

function loginUser(accessToken, dispatch, history) {
	localStorage.setItem('accessToken', accessToken);

	dispatch({ type: 'LOGIN_SUCCESS' });

	history.push('/app/dashboard');
}

function signOut(dispatch, history, callback) {
	new ApiClient().delete('/session', {}, (err) => {
		if (err) {
			callback();
			return toast('Se ha producido un error. Intentá nuevamente.', {type: 'error'});
		}
		localStorage.clear();
		dispatch({ type: 'SIGN_OUT_SUCCESS' });
		history.push('/login');
	});
}

function recoverPassword(callback) {
	new ApiClient().delete('/account/recover', {}, (err) => {
		if (err) {
			callback(err);
			return toast('Se ha producido un error. Intentá nuevamente.', {type: 'error'});
		}
		callback();
	});
}
