import React from 'react';
import { IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import useStyles from './styles';
import Modal from '@material-ui/core/Modal';

const MyModal = (props) => {

	const classes = useStyles();

	return (
		<Modal
			aria-labelledby="simple-modal-title"
			aria-describedby="simple-modal-description"
			disableEscapeKeyDown
			open={props.open}
		>
			<div className={classes.modal}>
				<IconButton
					className={classes.modalCloseButton}
					color="primary"
					onClick={props.onClose}
				>
					<CloseIcon/>
				</IconButton>
				{props.children}
			</div>
		</Modal>
	);

};

export default MyModal;
