import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({
	modal: {
		margin: '20px auto',
		width: '80%',
		maxWidth: 400,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4),
		position: 'relative',
		outline: 'none',
	},
	modalCloseButton: {
		position: 'absolute',
		top: 0,
		right: 0,
	},
}));
