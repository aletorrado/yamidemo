import React, { useState } from "react";
import {
	AppBar,
	Toolbar,
	IconButton,
	InputBase,
} from "@material-ui/core";
import {
	Menu as MenuIcon,
	Search as SearchIcon,
} from "@material-ui/icons";
import classNames from "classnames";

// styles
import useStyles from "./styles";

// components
import { Badge, Typography, Button } from "../Wrappers/Wrappers";

// context
import {
	toggleSidebar, useLayoutDispatch,
} from "../../context/LayoutContext";
import UserMenu from "../UserMenu";
import { useUserDispatch } from "../../context/UserContext";

const Header = (props) => {
	let classes = useStyles();

	// global
	let layoutDispatch = useLayoutDispatch();
	let userDispatch = useUserDispatch();

	// local
	let [isSearchOpen, setSearchOpen] = useState(false);

	return (
		<AppBar position="fixed" className={classes.appBar}>
			<Toolbar className={classes.toolbar}>
				<IconButton
					color="inherit"
					onClick={() => toggleSidebar(layoutDispatch)}
					className={classNames(
						classes.headerMenuButton,
						classes.headerMenuButtonCollapse,
					)}
				>
					<MenuIcon
						classes={{
							root: classNames(
								classes.headerIcon,
								classes.headerIconCollapse,
							),
						}}
					/>
				</IconButton>
				<Typography variant="h6" weight="medium" className={classes.logotype}>
					React Material Admin
				</Typography>
				<div className={classes.grow} />
				{props.search &&
				<div
					className={classNames(classes.search, {
						[classes.searchFocused]: isSearchOpen,
					})}
				>
					<div
						className={classNames(classes.searchIcon, {
							[classes.searchIconOpened]: isSearchOpen,
						})}
						onClick={() => setSearchOpen(!isSearchOpen)}
					>
						<SearchIcon classes={{ root: classes.headerIcon }} />
					</div>
					<InputBase
						placeholder="Search…"
						classes={{
							root: classes.inputRoot,
							input: classes.inputInput,
						}}
					/>
				</div>}
				<UserMenu userDispatch={userDispatch} />
			</Toolbar>
		</AppBar>
	);
};

export default Header;
