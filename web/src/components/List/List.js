import React from 'react';
import { CircularProgress, Typography } from '@material-ui/core';
import MUIDataTable from 'mui-datatables';
import cloneDeep from 'lodash/cloneDeep';
import { toast } from 'react-toastify';
import { withRouter } from 'react-router-dom';

import ApiClient from '../../api/api-client';
import { debounce, isFunction } from 'lodash';

class List extends React.Component {

	constructor(props) {
		super(props);

		let defaultFalseKeys = ['searchable', 'filter', 'sort'];
		this.props.columns.forEach((col) => {
			if (!col.options) {
				col.options = {};
			}
			let options = col.options;
			defaultFalseKeys.forEach((key) => {
				if (!(key in options)) {
					options[key] = false;
				}
			});
		});

		let data = this.props.data || [];
		this.state = {
			page: 0,
			count: data.length,
			data: data,
			isLoading: !!this.props.endpoint,
			searchText: '',
			sort: this.props.sort,
		};

		this.limit = 'limit' in this.props ? this.props.limit : 15;
		this.api = new ApiClient(this.props.baseUrl);

		this.onSearchChange = debounce(this.applyFilters, 1000);
	}

	componentDidMount() {
		if (this.props.endpoint) {
			this.getData();
		}
	}

	componentWillUnmount() {
		this.api.cancel();
	}

	applyFilters(newState) {
		newState.page = newState.page || 0;
		if (this.props.endpoint) {
			this.getData(newState);
		} else {
			this.setState(newState);
		}
	}

	getData(newState) {
		this.setState({ isLoading: true });
		newState = Object.assign(cloneDeep(this.state), newState || {});
		let params = this.props.params ? cloneDeep(this.props.params) : {};
		params.skip = newState.page * this.limit;
		params.limit = this.limit;
		params.search = newState.searchText || undefined;
		params.sort = newState.sort || undefined;
		this.api.get(this.props.endpoint, {
			params: params,
		}, (err, res) => {
			if (err) {
				return toast('Se ha producido un error. Intentá nuevamente.', { type: 'error' });
			}

			this.setState(Object.assign(newState, {
				isLoading: false,
				data: res.data,
				count: res.count,
			}));
		});
	}

	render() {
		const { data, page, count, isLoading, searchText, sort } = this.state;

		if (isLoading) {
			return (
				<div style={{
					display: 'flex',
					justifyContent: 'center'
				}}>
					<CircularProgress size={48} style={{ alignSelf: 'center' }}/>
				</div>
			);
		}

		const searchableColumns = this.props.columns
			.filter(col => {
				return col.options.searchable;
			});

		if (sort) {
			let asc = sort[0] === '-';
			let colName = asc ? sort.substr(1) : sort;
			this.props.columns.forEach((col) => {
				col.options.sortDirection = colName === col.name ? (asc ? 'asc' : 'desc') : 'none';
			});
		}

		const options = Object.assign({
			responsive: 'scrollMaxHeight',
			serverSide: !!this.props.endpoint,
			count: count,
			page: page,
			onChangePage: (page) => {
				this.applyFilters({
					page: page,
				});
			},
			sort: !!this.props.columns
				.filter(col => {
					return col.options.sort;
				}).length,
			onColumnSortChange: (changedColumn, direction) => {
				this.applyFilters({
					sort: direction === 'ascending' ? `-${changedColumn}` : changedColumn,
				});
			},
			searchText: searchText,
			onSearchChange: (searchText) => {
				this.onSearchChange({
					searchText: searchText,
				});
			},
			rowsPerPage: this.limit,
			pagination: !!this.limit,
			rowsPerPageOptions: [],
			download: this.props.download,
			print: false,
			selectableRowsHeader: false,
			selectableRows: false,
			onRowClick: this.props.readOnly || !this.props.rowLink ? null : (rowData, rowMeta) => {
				let row = data[rowMeta.dataIndex];
				let endpoint = isFunction(this.props.rowLink) ? this.props.rowLink(row) : this.props.rowLink;
				this.props.history.push(`${endpoint}/${row.id}`);
			},
			filter: !!this.props.columns
				.filter(col => {
					return col.options.filter;
				}).length,
			search: !!searchableColumns.length,
			searchPlaceholder: searchableColumns
				.map(col => {
					return col.label;
				})
				.join(', '),
			textLabels: {
				body: {
					noMatch: 'No se encontraron registros.',
					toolTip: 'Ordenar',
					columnHeaderTooltip: column => `Ordenar por ${column.label}`
				},
				pagination: {
					next: 'Siguiente',
					previous: 'Anterior',
					rowsPerPage: 'Resultados por página:',
					displayRows: 'de',
				},
				toolbar: {
					search: 'Buscar',
					downloadCsv: 'Descargar CSV',
					print: 'Imprimir',
					viewColumns: 'Ver columnas',
					filterTable: 'Filtrar',
				},
				filter: {
					all: 'Todos',
					title: 'FILTROS',
					reset: 'Resetear',
				},
				viewColumns: {
					title: 'Mostrar columnas',
					titleAria: 'Mostrar/ocultar columnas',
				},
				selectedRows: {
					text: 'seleccionado/s',
					delete: 'Eliminar',
					deleteAria: 'Eliminar seleccionados',
				},
			}
		}, this.props.options || {});

		return (
			<>
				<MUIDataTable
					title={
						<Typography variant="h6">
							{this.props.title}
							{isLoading && <CircularProgress size={24} style={{
								marginLeft: 15,
								position: 'relative',
								top: 4
							}}/>}
						</Typography>
					}
					data={data}
					columns={this.props.columns}
					options={options}/>
			</>
		);

	}
}

export default withRouter(List);
