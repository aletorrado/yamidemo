import React, { useState } from "react";
import {
	Paper,
	IconButton,
	Menu,
	Typography,
} from "@material-ui/core";
import { Add as AddIcon, MoreVert as MoreIcon } from "@material-ui/icons";
import classnames from "classnames";

// styles
import useStyles from "./styles";

/*
<MenuItem>
	<Typography>Editar</Typography>
</MenuItem>
 */

const Widget = ({
	children,
	title,
	noBodyPadding,
	bodyClass,
	menuItems,
	onAdd,
	header,
	...props
}) => {
	const classes = useStyles();

	// local
	let [moreButtonRef, setMoreButtonRef] = useState(null);
	let [isMoreMenuOpen, setMoreMenuOpen] = useState(false);

	return (
		<div className={classes.widgetWrapper}>
			<Paper className={classes.paper} classes={{ root: classes.widgetRoot }}>
				<div className={classes.widgetHeader}>
					{header ? (
						header
					) : (
						<React.Fragment>
							<Typography variant="h5" color="textSecondary">
								{title}
							</Typography>
							{onAdd && (
								<IconButton
									color="primary"
									classes={{ root: classes.widgetButton }}
									onClick={onAdd}
								>
									<AddIcon />
								</IconButton>
							)}
							{menuItems && (
								<IconButton
									color="primary"
									classes={{ root: classes.widgetButton }}
									aria-owns="widget-menu"
									aria-haspopup="true"
									onClick={() => setMoreMenuOpen(true)}
									buttonRef={setMoreButtonRef}
								>
									<MoreIcon />
								</IconButton>
							)}
						</React.Fragment>
					)}
				</div>
				<div
					className={classnames(classes.widgetBody, {
						[classes.noPadding]: noBodyPadding,
						[bodyClass]: bodyClass,
					})}
				>
					{children}
				</div>
			</Paper>
			<Menu
				id="widget-menu"
				open={isMoreMenuOpen}
				anchorEl={moreButtonRef}
				onClose={() => setMoreMenuOpen(false)}
				disableAutoFocusItem
			>
				{menuItems}
			</Menu>
		</div>
	);
};

export default Widget;
