import React from 'react';
import { Fab, IconButton, Menu, MenuItem } from '@material-ui/core';
import { Badge, Typography } from '../Wrappers';
import {
	MailOutline as MailIcon,
	NotificationsNone as NotificationsIcon,
	Person as AccountIcon, Send as SendIcon
} from '@material-ui/icons';
import UserAvatar from '../UserAvatar';
import Notification from '../Notification';
import { signOut } from '../../context/UserContext';
import ApiClient from '../../api/api-client';
import withStyles from './styles';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import compose from 'recompose/compose';


class UserMenu extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			profile: {
				menu: null,
				data: {},
			},
			messages: {
				data: [],
				unread: 0,
				menu: null,
			},
			notifications: {
				data: [],
				unread: 0,
				menu: null,
			},
			isSigningOut: false,
		};
	}

	componentDidMount() {
		new ApiClient().get('/user', {}, (err, data) => {
			if (err) {
				return;
			}

			let newState = {
				profile: {
					...this.state.profile,
					data: data.profile,
				},
			};
			if (this.props.notifications) {
				newState.notifications = {
					...this.state.notifications,
					...data.notifications,
				};
			}
			if (this.props.messages) {
				newState.messages = {
					...this.state.messages,
					...data.messages,
				};
			}
			this.setState(newState);
		});
	}

	render() {
		const { classes } = this.props;
		const { messages, notifications, profile } = this.state;

		return (
			<div>
				{this.props.notifications &&
				<IconButton
					color="inherit"
					aria-haspopup="true"
					aria-controls="header-dropdown-menu"
					onClick={e => {
						this.setState({
							notifications: {
								...notifications,
								unread: 0,
								menu: e.currentTarget,
							},
						});
					}}
					className={classes.headerMenuButton}
				>
					<Badge
						badgeContent={notifications.unread || null}
						color="warning"
					>
						<NotificationsIcon classes={{ root: classes.headerIcon }}/>
					</Badge>
				</IconButton>
				}
				{this.props.messages &&
				<IconButton
					color="inherit"
					aria-haspopup="true"
					aria-controls="header-dropdown-menu"
					onClick={e => {
						this.setState({
							messages: {
								...messages,
								menu: e.currentTarget,
								unread: 0,
							},
						});
					}}
					className={classes.headerMenuButton}
				>
					<Badge
						badgeContent={messages.unread || null}
						color="secondary"
					>
						<MailIcon classes={{ root: classes.headerIcon }}/>
					</Badge>
				</IconButton>
				}
				<IconButton
					aria-haspopup="true"
					color="inherit"
					className={classes.headerMenuButton}
					aria-controls="profile-menu"
					onClick={e => this.setState({
						profile: {
							...profile,
							menu: e.currentTarget,
						}
					})}
				>
					<AccountIcon classes={{ root: classes.headerIcon }}/>
				</IconButton>

				{this.props.notifications &&
				<Menu
					id="notifications-menu"
					open={Boolean(notifications.menu)}
					anchorEl={notifications.menu}
					onClose={() => this.setState({
						notifications: {
							...notifications,
							menu: null
						}
					})}
					className={classes.headerMenu}
					disableAutoFocusItem
				>
					{notifications.data.map(notification => (
						<MenuItem
							key={notification.id}
							onClick={() => this.setState({
								...notifications,
								notifications: { menu: null }
							})}
							className={classes.headerMenuItem}>
							<Notification {...notification} typographyVariant="inherit"/>
						</MenuItem>
					))}
				</Menu>}

				{this.props.messages &&
				<Menu
					id="header-dropdown-menu"
					open={Boolean(messages.menu)}
					anchorEl={messages.menu}
					onClose={() => this.setState({
						messages: {
							...messages,
							menu: null
						}
					})}
					MenuListProps={{ className: classes.headerMenuList }}
					className={classes.headerMenu}
					classes={{ paper: classes.profileMenu }}
					disableAutoFocusItem
				>
					<div className={classes.profileMenuUser}>
						<Typography variant="h4" weight="medium">
							Nuevos mensajes
						</Typography>
						<Typography
							className={classes.profileMenuLink}
							component="a"
							color="secondary"
						>
							{messages.length} Nuevos mensajes
						</Typography>
					</div>
					{messages.data.map(message => (
						<MenuItem key={message.id} className={classes.messageNotification}>
							<div className={classes.messageNotificationSide}>
								<UserAvatar color={message.variant} name={message.name}/>
								<Typography size="sm" color="text" colorBrightness="secondary">
									{message.time}
								</Typography>
							</div>
							<div
								className={classNames(
									classes.messageNotificationSide,
									classes.messageNotificationBodySide,
								)}>
								<Typography weight="medium" gutterBottom>
									{message.name}
								</Typography>
								<Typography color="text" colorBrightness="secondary">
									{message.message}
								</Typography>
							</div>
						</MenuItem>
					))}
					<Fab
						variant="extended"
						color="primary"
						aria-label="Add"
						className={classes.sendMessageButton}
					>
						Send New Message
						<SendIcon className={classes.sendButtonIcon}/>
					</Fab>
				</Menu>}

				<Menu
					id="profile-menu"
					open={Boolean(profile.menu)}
					anchorEl={profile.menu}
					onClose={() => this.setState({
						profile: {
							...profile,
							menu: null,
						}
					})}
					className={classes.headerMenu}
					classes={{ paper: classes.profileMenu }}
					disableAutoFocusItem
				>
					<div className={classes.profileMenuUser}>
						<Typography variant="h4" weight="medium">
							{profile.data.displayName}
						</Typography>
					</div>
					<MenuItem
						className={classNames(
							classes.profileMenuItem,
							classes.headerMenuItem,
						)}
					>
						<AccountIcon className={classes.profileMenuIcon}/> Perfil
					</MenuItem>
					{this.props.messages &&
					<MenuItem
						className={classNames(
							classes.profileMenuItem,
							classes.headerMenuItem,
						)}
					>
						<AccountIcon className={classes.profileMenuIcon}/> Mensajes
					</MenuItem>}
					<div className={classes.profileMenuUser}>
						<Typography
							className={classes.profileMenuLink}
							color="primary"
							onClick={() => {
								if (this.state.isSigningOut) {
									return;
								}
								this.setState({isSigningOut: true});
								signOut(this.props.userDispatch, this.props.history, () => {
									this.setState({isSigningOut: false});
								});
							}}
						>
							Cerrar sesión
						</Typography>
					</div>
				</Menu>
			</div>
		);
	}

}

export default compose(
	withRouter,
	withStyles,
)(UserMenu);
