import React from 'react';
import FormComponent from './FormComponent';
import { TextField } from '@material-ui/core';
import {
	MAX_DESCRIPTION_LENGTH,
	MAX_NUMBER_LENGTH,
	MAX_STRING_LENGTH
} from '../../../utils/string-utils';

class FormTextField extends FormComponent {

	constructor(props) {
		super(props);
	}

	addValidators(validators, attrs) {
		super.addValidators(validators, attrs);

		let isInteger = attrs.type === 'integer';
		let type = isInteger || attrs.type === 'currency' ? 'number' : attrs.type || 'text';

		if (validators.length) {
			validators.minLength = validators.length;
			validators.maxLength = validators.length;
			delete validators.length;
		}

		const isNumber = type === 'number';
		let maxLength = validators.maxLength ||
			(isNumber ? MAX_NUMBER_LENGTH :
				(attrs.multiline ? MAX_DESCRIPTION_LENGTH : MAX_STRING_LENGTH)
			);
		if (isNumber) {
			if (validatos.max === undefined) {
				validatos.max = parseInt(new Array(maxLength + 1).join('9')) + (isInteger ? 0 : 0.99);
			}
		} else {
			validators.maxLength = maxLength;
		}

		if (attrs.type === 'email') {
			let emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			validators.pattern = {
				value: emailPattern,
				message: 'Email inválido.',
			};
		} else if (attrs.type === 'url') {
			let urlPattern = new RegExp(
				"^" +
				"(?:(?:https?|ftp)://)?" + // protocol identifier
				"(?:\\S+(?::\\S*)?@)?" + // user:pass authentication
				"(?:" +
				("(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
					"(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
					"(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})") +
				// IP address dotted notation octets
				// excludes loopback network 0.0.0.0
				// excludes reserved space >= 224.0.0.0
				// excludes network & broadcast addresses
				// (first & last IP address of each class)
				"(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
				"(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
				"(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
				"|" +
				"(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)" + // host name
				"(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*" + // domain name
				"(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" + // TLD identifier
				")" +
				"(?::\\d{2,5})?" + // port number
				"(?:/[^\\s]*)?" + // resource path
				"$", "i"
			);
			validators.pattern = {
				value: urlPattern,
				message: 'URL inválida.',
			};
		}

		attrs.type = type;
	}

	render() {
		return (
			<TextField
				{...this.getAttrs()}
			/>
		);
	}

}

export default FormTextField;
