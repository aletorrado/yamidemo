import React  from 'react';
import PropTypes from 'prop-types';

class FormComponent extends React.Component {

	constructor(props) {
		super(props);
	}

	getAttrs() {
		const {form, validators = {}, ...attrs} = this.props;
		this.addValidators(validators, attrs);
		attrs.error = !!form.errors[attrs.name];
		attrs.helperText = form.errors[attrs.name]?.message ?? this.props.helperText;
		attrs.inputRef = form.register(validators);
		return attrs;
	}

	addValidators(validators, attrs) {
		if (attrs.required) {
			validators.required = 'Campo requerido.';
		}
	}

}

FormComponent.propTypes = {
	name: PropTypes.string.isRequired,
	form: PropTypes.any.isRequired,
};

export default FormComponent;
