import { InputAdornment } from '@material-ui/core';
import { RemoveRedEye } from '@material-ui/icons';
import React  from 'react';
import FormTextField from './FormTextField';
import FormComponent from './FormComponent';


class FormPasswordField extends FormComponent {
	constructor(props) {
		super(props);

		this.state = {
			passwordIsMasked: true,
		};
	}

	togglePasswordMask = () => {
		this.setState(prevState => ({
			passwordIsMasked: !prevState.passwordIsMasked,
		}));
	};

	render() {
		const { passwordIsMasked } = this.state;

		return (
			<FormTextField
				{...this.props}
				type={passwordIsMasked ? 'password' : 'text'}
				InputProps={{
					endAdornment: (
						<InputAdornment position="end">
							<RemoveRedEye
								style={{cursor: 'pointer'}}
								onClick={this.togglePasswordMask}
							/>
						</InputAdornment>
					),
					...this.props.InputProps
				}}
			/>
		);
	}
}

export default FormPasswordField;
