import React from 'react';
import {
	DatePicker,
	MuiPickersUtilsProvider
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from '../../../utils/date-utils';
import moment from 'moment';
import FormComponent from './FormComponent';
import PropTypes from 'prop-types';
import { BaseValidationProps } from '@material-ui/pickers/_helpers/text-field-helper';

class FormDatePicker extends FormComponent {

	constructor(props) {
		super(props);

		this.state = {
			date: null,
		};
	}

	render() {
		const { form, name, defaultValue } = this.props;
		let date = form.watch(name);
		return (
			<MuiPickersUtilsProvider utils={MomentUtils}>
				<DatePicker
					format={DATE_FORMAT}
					value={this.state.date || date && moment(date, DATE_FORMAT) || defaultValue}
					onChange={(x) => {
						this.setState({ date: x });
					}}
					onBlur={() => form.clearError(name)}
					onOpen={() => form.clearError(name)}
					{...this.getAttrs()}
				/>
			</MuiPickersUtilsProvider>
		);
	}
}

FormDatePicker.propTypes = {
	defaultValue: PropTypes.object,
};

export default FormDatePicker;
