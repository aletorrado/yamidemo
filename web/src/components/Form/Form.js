import React from 'react';
import { Button, CircularProgress, Grid, Typography } from '@material-ui/core';
import { toast } from 'react-toastify';
import { withRouter } from 'react-router-dom';

import ApiClient from '../../api/api-client';
import Box from '@material-ui/core/Box';
import { isFunction, kebabCase } from 'lodash';
import Paper from '@material-ui/core/Paper';
import compose from 'recompose/compose';
import withStyles from './styles';
import PropTypes from 'prop-types';
import ConditionalWrapper from '../ConditionalWrapper';

class Form extends React.Component {

	constructor(props) {
		super(props);

		this.docId = this.props.match.params.id;
		this.endpoint = this.props.endpoint || kebabCase(this.props.location.pathname);

		this.state = {
			isLoading: !!this.docId,
			isSubmitting: false,
			isDeleting: false,
		};

		this.api = new ApiClient(this.props.baseUrl);
	}

	componentDidMount() {
		if (this.docId) {
			this.getData();
		}/* else {
			let data = {
				firstName: 'Yamila',
				lastName: 'Fraiman',
				birthdate: '1989-12-07',
				address: 'Lacroze 2542'
			};
			this.props.parseResponse && this.props.parseResponse(data);
			for (const [key, value] of Object.entries(data)) {
				this.props.form.setValue(key, value);
			}
		}*/
	}

	getData() {
		this.setState({ isLoading: true });
		let url = this.props.endpoint;
		if (this.docId) {
			url += `/${this.docId}`;
		}
		this.api.get(url, {}, (err, res) => {
			if (err) {
				return toast('Se ha producido un error. Intentá nuevamente.', { type: 'error' });
			}

			this.props.parseResponse && this.props.parseResponse(res);
			for (const [key, value] of Object.entries(res)) {
				this.props.form.setValue(key, value);
			}

			this.setState({ isLoading: false });
		});
	};

	handleBack = (values) => {
		if (this.props.onBack && this.props.onBack()) {
			return;
		}

		this.props.history.goBack()
	};

	handleSubmit = (values) => {
		this.setState({ isSubmitting: true });

		let url = this.props.endpoint;
		if (this.docId) {
			url += `/${this.docId}`;
		}
		let params = isFunction(this.props.formatParams) ? this.props.formatParams(values) : values;
		this.api.post(url, {
			data: params,
		}, (err, res) => {
			if (err) {
				this.setState({ isSubmitting: false });
				if (this.props.onError && this.props.onError(err)) {
					return;
				}
				return toast('Se ha producido un error. Intentá nuevamente.', { type: 'error' });
			}

			if (this.props.onSuccess && this.props.onSuccess(res)) {
				return;
			}

			toast('Los cambios han sido guardados correctamente.', {type: 'success'});

			if (!this.docId && this.props.parent) {
				setTimeout(
					this.props.history.push(this.props.parent),
					2000
				);
				return;
			}

			this.setState({ isSubmitting: false });
		});
	};

	handleDelete = () => {
		this.setState({ isDeleting: true });

		if (!confirm('¿Desea continuar con la eliminación?')) {
			this.setState({ isDeleting: false });
			return;
		}
		this.api.delete(url, {}, (err, res) => {
			if (err) {
				return toast('Se ha producido un error. Intentá nuevamente.', { type: 'error' });
			}

			this.props.history.push(this.props.parent);
		});
	};

	render() {
		const { classes } = this.props;
		const { handleSubmit, isFormSubmitting } = this.props.form;
		const { isLoading, isDeleting } = this.state;

		const isSubmitting = this.state.isSubmitting || isFormSubmitting;
		const disableButtons = isSubmitting || isDeleting;

		if (isLoading) {
			return (
				<div style={{
					display: 'flex',
					justifyContent: 'center'
				}}>
					<CircularProgress size={48} style={{ alignSelf: 'center' }}/>
				</div>
			);
		}

		return (
			<Grid container spacing={4} mt={2}>
				<Grid item xs={12}>
					<ConditionalWrapper
						condition={!this.props.noBackground}
						wrapper={children => <Paper className={classes.paper} classes={{ root: classes.widgetRoot }}>{children}</Paper>}
					>
						<>
							{this.props.title && <Typography variant="h5" color="textSecondary">
								{this.props.title}
							</Typography>}

							<form
								noValidate
								onKeyPress={(e) => {
									if (e.which === 13 && e.target.type !== 'textarea') {
										e.preventDefault();
									}
								}}
								id={this.props.id}
								autoComplete="off"
								onSubmit={handleSubmit(this.handleSubmit)}>

								{this.props.children}

								<div style={{ width: '100%' }}>
									<Box display="flex" mt={4}>
										<Box flexGrow={1}>
											<div>
												{!!this.docId && !this.props.readOnly && !this.props.protected &&
													(isDeleting ?
															<CircularProgress size={26}/> :
															<Button
																variant="text"
																onClick={this.handleDelete}
																disabled={disableButtons}
															>{this.props.deleteText || 'Eliminar'}</Button>
													)
												}
											</div>
										</Box>
										<Box>
											<div>
												{!this.props.hideBack && (!!this.props.history.length || !!this.props.onBack) && <Button
													variant="text"
													onClick={handleBack}
													disabled={isDeleting}
												>Volver</Button>}
											</div>
										</Box>
										<Box ml={3}>
											<div>
												{!this.props.readOnly &&
													(isSubmitting ?
														<CircularProgress size={26}/> :
														<Button
															type="submit"
															variant="contained"
															color="secondary"
															disabled={disableButtons}
														>{this.props.submitText || 'Guardar'}</Button>
													)
												}
											</div>
										</Box>
									</Box>
								</div>
							</form>
						</>
					</ConditionalWrapper>
				</Grid>
			</Grid>
		);

	}
}

Form.propTypes = {
	form: PropTypes.any.isRequired,
	noBackground: PropTypes.bool,
	title: PropTypes.string,
};

export default compose(
	withRouter,
	withStyles,
)(Form);
