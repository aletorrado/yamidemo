import { withStyles } from "@material-ui/styles";

export default withStyles(theme => ({
  button: {
    boxShadow: theme.customShadows.widget,
    textTransform: "none",
    "&:active": {
      boxShadow: theme.customShadows.widgetWide,
    },
  },
  widgetRoot: {
    boxShadow: theme.customShadows.widget,
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    overflow: "hidden",
    padding: theme.spacing(3),
  },
}));
