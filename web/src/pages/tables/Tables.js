import React from 'react';
import { Grid } from '@material-ui/core';

// components
import PageTitle from '../../components/PageTitle';
import List from '../../components/List';

const columns = [
	{
		name: 'id',
		label: 'ID',
		options: {
			searchable: true,
			sort: true,
		},
	},
	{
		name: 'name',
		label: 'Nombre',
		options: {
			searchable: true,
			sort: true,
		},
	},
	{
		name: 'lastName',
		label: 'Apellido'
	},
];

const Tables = () => {
	return (
		<>
			<PageTitle title="Tables"/>

			<Grid container spacing={4}>
				<Grid item xs={12}>
					<List
						rowLink="/app/forms"
						columns={columns}
						baseUrl="https://api.myjson.com/bins"
						endpoint="/evos2"
						sort="id"/>
				</Grid>
			</Grid>
		</>
	);
};

export default Tables;
