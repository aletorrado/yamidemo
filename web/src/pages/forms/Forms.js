import React, { useState } from 'react';
import { Grid } from '@material-ui/core';

import PageTitle from '../../components/PageTitle';
import Form from '../../components/Form';
import moment from 'moment';
import { useForm } from 'react-hook-form';
import FormDatePicker from '../../components/Form/components/FormDatePicker';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from '../../utils/date-utils';
import FormTextField from '../../components/Form/components/FormTextField';

const Forms = () => {

	const form = useForm();

	const parseResponse = (res) => {
		res.birthdate = moment(res.birthdate, SERVER_DATE_FORMAT)
			.format(DATE_FORMAT);
	};

	const formatParams = (params) => {
		params.birthdate = moment(params.birthdate, DATE_FORMAT)
			.format(SERVER_DATE_FORMAT);
	};

	return (
		<>
			<PageTitle title="Forms"/>

			<Form
				title="Form data"
				parent="/doc"
				form={form}
				parseResponse={parseResponse}
				formatParams={formatParams}
			>
				<Grid container spacing={3}>
					<Grid item xs={12} sm={6}>
						<FormTextField
							form={form}
							name="firstName"
							label="Nombre"
							fullWidth
							required
						/>
					</Grid>
					<Grid item xs={12} sm={6}>
						<FormTextField
							form={form}
							name="lastName"
							label="Apellido"
							fullWidth
							required
						/>
					</Grid>
					<Grid item xs={12}>
						<FormTextField
							form={form}
							name="address"
							label="Dirección"
							fullWidth
							required
						/>
					</Grid>
					<Grid item xs={12}>
						<FormDatePicker
							form={form}
							name="birthdate"
							label="Fecha de nacimiento"
							fullWidth
							required
							disableFuture
							defaultValue={moment()
								.subtract({ years: 18 })}
							InputLabelProps={{
								shrink: true,
							}}
						/>
					</Grid>
				</Grid>
			</Form>
		</>
	);
};

export default Forms;
