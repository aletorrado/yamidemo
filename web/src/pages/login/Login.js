import React, { useState } from 'react';
import {
	Grid,
	Typography,
	Button,
	Tabs,
	Tab,
	IconButton,
} from '@material-ui/core';
import { withRouter } from 'react-router-dom';

// styles
import useStyles from './styles';

// logo
import logo from './logo.svg';

// context
import { useUserDispatch, loginUser, recoverPassword } from '../../context/UserContext';
import moment from 'moment';
import { useForm } from 'react-hook-form';
import Form from '../../components/Form';
import FormPasswordField from '../../components/Form/components/FormPasswordField';
import FormTextField from '../../components/Form/components/FormTextField';
import { toast } from 'react-toastify';
import MyModal from '../../components/MyModal';

const Login = (props) => {
	const classes = useStyles();

	const loginForm = useForm();
	const signUpForm = useForm();
	const passwordRecoveryForm = useForm();

	// global
	const userDispatch = useUserDispatch();

	// local
	let [activeTabId, setActiveTabId] = useState(0);
	let [isPasswordRecoveryOpen, setPasswordRecoveryOpen] = useState(false);

	let startYear = 2020;
	let currentYear = moment()
		.year();

	return (
		<Grid container className={classes.container}>
			<div className={classes.logotypeContainer}>
				<img src={logo} alt="logo" className={classes.logotypeImage}/>
				<Typography className={classes.logotypeText}>Material Admin</Typography>
			</div>
			<div className={classes.formContainer}>
				<div className={classes.form}>
					<Tabs
						value={activeTabId}
						onChange={(e, id) => setActiveTabId(id)}
						indicatorColor="primary"
						textColor="primary"
						centered
						className={classes.tabs}
					>
						<Tab label="Ingresar" classes={{ root: classes.tab }}/>
						<Tab label="Nuevo usuario" classes={{ root: classes.tab }}/>
					</Tabs>
					{activeTabId === 0 && (
						<>
							<Form
								hideBack
								form={loginForm}
								endpoint="/session"
								submitText="Ingresar"
								noBackground
								onSuccess={(res) => {
									loginUser(
										res.accessToken,
										userDispatch,
										props.history,
									);
									return true;
								}}
							>
								<FormTextField
									name="username"
									form={loginForm}
									margin="normal"
									placeholder="Email"
									type="email"
									fullWidth
									required
								/>
								<FormPasswordField
									name="password"
									form={loginForm}
									margin="normal"
									placeholder="Contraseña"
									fullWidth
									required
								/>
							</Form>
							<div className={classes.formButtons}>
								<Button
									color="primary"
									size="large"
									className={classes.forgetButton}
									onClick={e => {
										setPasswordRecoveryOpen(true);
									}}
								>
									Olvidé mi contraseña
								</Button>
								<MyModal
									open={isPasswordRecoveryOpen}
									onClose={() => {
										setPasswordRecoveryOpen(false);
									}}
								>
									<Form
										title="Recuperar contraseña"
										noBackground
										hideBack
										form={passwordRecoveryForm}
										endpoint="/account/recover"
										submitText="Enviar"
										onSuccess={() => {
											toast('Te hemos enviado un email para recuperar tu contraseña.', {type: 'success'});
											setPasswordRecoveryOpen(false);
											return true;
										}}
										onError={(err) => {
											if (err.response?.status === 404) {
												passwordRecoveryForm.setError('username', 'notMatch', 'El email no se encuentra registrado.')
											}
										}}
									>
										<FormTextField
											name="username"
											form={passwordRecoveryForm}
											margin="normal"
											placeholder="Email"
											type="email"
											fullWidth
											required
										/>
									</Form>
								</MyModal>
							</div>
						</>
					)}
					{activeTabId === 1 && (
						<React.Fragment>
							<Form
								hideBack
								form={signUpForm}
								submitText="Registrarme"
								noBackground
								endpoint="/account"
								onSuccess={() => {
									setActiveTabId(0);
									toast('Gracias por registrarte. Pronto recibirás un email.');
									return true;
								}}
							>
								<FormTextField
									name="firstName"
									form={signUpForm}
									margin="normal"
									placeholder="Nombre"
									fullWidth
									required
								/>
								<FormTextField
									name="lastName"
									form={signUpForm}
									margin="normal"
									placeholder="Apellido"
									fullWidth
									required
								/>
								<FormTextField
									name="email"
									form={signUpForm}
									margin="normal"
									placeholder="Email"
									type="email"
									fullWidth
									required
								/>
								<FormPasswordField
									name="password"
									form={signUpForm}
									margin="normal"
									placeholder="Contraseña"
									fullWidth
									required
								/>
							</Form>
						</React.Fragment>
					)}
				</div>
				<Typography color="primary" className={classes.copyright}>
					© {startYear} {startYear !== currentYear && ' - ' + currentYear} 99UNO, LLC.
					Todos los derechos reservados.
				</Typography>
			</div>
		</Grid>
	);
};

export default withRouter(Login);
