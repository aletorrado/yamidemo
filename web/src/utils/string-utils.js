export const MAX_STRING_LENGTH = 256;
export const MAX_DESCRIPTION_LENGTH = 5000;
export const MAX_NUMBER_LENGTH = 15;
