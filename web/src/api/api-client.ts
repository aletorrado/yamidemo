import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelTokenSource, Method} from 'axios';

const BASE_URL = '/api/v1';
const CancelToken = axios.CancelToken;

class ApiClient {

    private source: CancelTokenSource;
    private api: AxiosInstance;

    constructor(baseUrl?: string) {
        this.source = CancelToken.source();
        this.api = axios.create({
            baseURL: baseUrl ?? BASE_URL,
            cancelToken: this.source.token
        });

        if (!baseUrl) {
            this.api.interceptors.request.use((config: AxiosRequestConfig) => {
                if (localStorage.accessToken) {
                    config.headers.Authorization = `Bearer ${localStorage.accessToken}`;
                }
                return config;
            }, (err: any) => {
                return Promise.reject(err);
            });
        }

        this.api.interceptors.response.use((res: AxiosResponse<any>) => {
            if (res.status >= 200 && res.status < 300) {
                return res.data;
            }

            const error = new Error(`HTTP Error ${res.statusText}`);
            console.error(error);
            return Promise.reject(error);
        }, (err: any) => {
            if (err.response?.status === 401 && localStorage.accessToken) {
                this._logout();
            }
            return Promise.reject(err);
        });
    }

    _exec(req: Promise<AxiosResponse>, callback?: (err?: any, res?: any) => void) {
        req
            .then((res: any) => {
                callback?.(null, res);
            })
            .catch((err: any) => {
                if (!axios.isCancel(err)) {
                    callback?.(err);
                }
            });
    }

    get(url: string, config: ApiClientConfig = {}, callback?: (err?: any, res?: any) => void) {
        this._exec(this.api.get(url, config), callback);
    }

    post(url: string, config: ApiClientConfig = {}, callback?: (err?: any, res?: any) => void) {
        this._exec(this.api.post(url, config.data, config), callback);
    }

    delete(url: string, config: ApiClientConfig = {}, callback?: (err?: any, res?: any) => void) {
        this._exec(this.api.delete(url, config), callback);
    }

    cancel() {
        this.source.cancel();
    }

    _logout() {
        localStorage.clear();
        window.location.reload();
    }

}

export interface ApiClientConfig {
    data?: {};
    params?: {};
    headers?: {};
}

export default ApiClient;
