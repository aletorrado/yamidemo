'use strict';
const cron = require('cron');
const domain = require('domain');

const mailer = require('./mailer');

module.exports = (db) => {

	const sendEmail = (opts, callback) => {
		opts.dummy = process.env.NODE_ENV !== 'production';
		opts.db = db;
		mailer.send(opts, callback);
	};

	let myDomain = domain.create();

    myDomain.on('error', (err) => {
		console.error('Scheduler error.', err && err.stack);
	});

	myDomain.run(() => {
		new cron.CronJob({
			cronTime: '00 03 * * *', //Every day 12 AM
			onTick: () => {
				console.log('Daily 12 AM Cron');
			},
			start: true
		});
	});
};
